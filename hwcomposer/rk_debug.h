/*
  HWC_LOG
    This is the facility to control debug log output .

  USAGE:
    property 'sys.hwc.log' 
      is sampled at the beginning of hwc_prepare() by calling HWC_LOG_START,
      and 
      is reset to zero at the end of hwc_set() by calling HWC_LOG_START.
  
    During that period, is_out_log() returns 0 or 1, depending on the properties('sys.hwc.log') value.
  
    So, the following command on the target board console (using adb etc) generates logs 
    during only one video frame.
  
    command : setprop sys.hwc.enable 1 <enter>
    
*/

/*
  xxx_TIME_ [DECLARE | BEGIN | END]
    This is the facility to measure elapsed time.
    ( xxx == [BLIT | HWC | FENCE] )

  USAGE:
    At apropriate place, declare variables by calling xxx_TIME_DECLARE.
    At the start point of measuring, call xxx_TIME_BEGIN.
    At the stop point, call xxx_TIME_END(limit,msg).

    IF the elapsed time exeeds 'limit' ms THEN 'msg' is executed.
    The calculated elapled time xxx_diff(unit=ms) can be used in 'msg'.
    (xxx == [blit | hwc | fence])

  LIMITATION:
    To calculate division by 10^3, it uses '>>10' instead of '/1000'.
    So the calculated elapled time is not precise.
*/

/*
  These functions are implemented in rk312x_hwcomposer.cpp and rk_hwcomposer.cpp
*/
extern void stop_debug_log(void);
extern void is_debug_log(void);
extern int is_out_log(void);


// log on/off
#define HWC_LOG_START is_debug_log();
#define HWC_LOG_STOP  stop_debug_log()
#define HWC_LOG(msg)  if(is_out_log()) {msg;} 

// measure elapsed time
//...common
#define TIME_DECLARE(prefix)   \
  struct timespec prefix ## _ts_beg, prefix ## _ts_end; long prefix ## _diff;
#define TIME_BEGIN(prefix)   \
  clock_gettime( CLOCK_MONOTONIC, &prefix ##_ts_beg );
#define TIME_END(prefix,limit,msg)                              \
  clock_gettime( CLOCK_MONOTONIC, &prefix ##_ts_end );  \
  if ( (prefix##_diff = \
        ((prefix##_ts_end.tv_sec  - prefix##_ts_beg.tv_sec )<<10) +      \
        ((prefix##_ts_end.tv_nsec - prefix##_ts_beg.tv_nsec)>>20)) > limit ) msg;
//...for blitter
#if hwcBlitUseTime
#define BLIT_TIME_DECLARE TIME_DECLARE(blit)
#define BLIT_TIME_BEGIN   TIME_BEGIN(blit)
#define BLIT_TIME_END(limit,msg)   TIME_END(blit,limit,msg)
#else
#define BLIT_TIME_DECLARE 
#define BLIT_TIME_BEGIN   
#define BLIT_TIME_END(limit,msg)
#endif
//...for general
#if hwcUseTime
#define HWC_TIME_DECLARE TIME_DECLARE(hwc)
#define HWC_TIME_BEGIN   TIME_BEGIN(hwc)
#define HWC_TIME_END(limit,msg)   TIME_END(hwc,limit,msg)
#else
#define HWC_TIME_DECLARE 
#define HWC_TIME_BEGIN   
#define HWC_TIME_END(limit,msg)
#endif
//... for fence
#if FENCE_TIME_USE
#define FENCE_TIME_DECLARE TIME_DECLARE(fence)
#define FENCE_TIME_BEGIN   TIME_BEGIN(fence)
#define FENCE_TIME_END(limit,msg)   TIME_END(fence,limit,msg)
#else
#define FENCE_TIME_DECLARE 
#define FENCE_TIME_BEGIN   
#define FENCE_TIME_END(limit,msg)
#endif
