This directory contains the rockchip's video driver source code for reference purposes.
It's part of fireprime_android5.1_git_20180510.tar.gz.
To get the complete kernel and driver source code, please visit the following links:

  http://wiki.t-firefly.com/en/Firefly-RK3128/Starter_Guide.html
  http://en.t-firefly.com/doc/download/page/id/6.html
  https://drive.google.com/drive/folders/1A9bkkJAKieTeUdPhTqGQbLD4nwMJr6_W
