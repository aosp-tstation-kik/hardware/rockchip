#if 0
  /* Check yuv format. */
  yuvFormat = (srcFormat >= RK_FORMAT_YCbCr_422_SP && srcFormat <= RK_FORMAT_YCbCr_420_P);

  /* Check stretching. */

  if(yuvFormat) {
      dstRect.left -= dstRect.left%2;
      dstRect.top -= dstRect.top%2;            
      dstRect.right -= dstRect.right%2;
      dstRect.bottom -= dstRect.bottom%2;
    }
    
  if ((Src->transform == HWC_TRANSFORM_ROT_90) || (Src->transform == HWC_TRANSFORM_ROT_270)) {
      hfactor = (float)(srcRect.right - srcRect.left) / (dstRect.bottom - dstRect.top);
      vfactor= (float)(srcRect.bottom - srcRect.top) / (dstRect.right - dstRect.left);
    } else {
      hfactor = (float)(srcRect.right - srcRect.left) / (dstRect.right - dstRect.left);
      vfactor = (float)(srcRect.bottom - srcRect.top) / (dstRect.bottom - dstRect.top);
    }

  if (hfactor < 1 || vfactor < 1)  // scale up use bicubic
    scale_mode = 2;
  stretch = (hfactor != 1.0f) || (vfactor != 1.0f);
  if (stretch && (srcFormat == RK_FORMAT_RGBA_8888 || srcFormat == RK_FORMAT_BGRA_8888)) {
      scale_mode = 0;     //  force change scale_mode to 0 ,for rga not support
    }


  /* Go through all visible regions (clip rectangles?). */
  for ( int n = 0 ; n < (int)Region->numRects ; ) {
  //int n = 0;
  //do {
      /* 16 rectangles a batch. */
      int m = 0;
      hwcRECT srcRects[16];
      hwcRECT dstRects[16];
      hwc_rect_t const * rects = Region->rects;

      for ( m = 0 ; n < (int)Region->numRects && m < 16; n++ ) {
          /* Hardware will mirror in dest rect and blit area in clipped rect.
           * But we need mirror area in clippred rect.
           * NOTE: Now we always set dstRect to clip area. */

          /* Intersect clip with dest. */
          dstRects[m].left   = hwcMAX(dstRect.left,   rects[n].left);
          dstRects[m].top    = hwcMAX(dstRect.top,    rects[n].top);
          dstRects[m].right  = hwcMIN(dstRect.right,  rects[n].right);
          dstRects[m].right  = hwcMIN(dstRects[m].right, (int)dstWidth);//dstWidth to signed int  can't be negative
          dstRects[m].bottom = hwcMIN(dstRect.bottom, rects[n].bottom);
          if (dstRects[m].top < 0) {// @ buyudaren grame dstRects[m].top < 0,bottom is height ,so do this
              dstRects[m].top = dstRects[m].top + dstHeight;
              dstRects[m].bottom = dstRects[m].bottom + dstRects[m].top;
            }

          /* Check dest area. */
          if ((dstRects[m].right <= dstRects[m].left) || (dstRects[m].bottom <= dstRects[m].top) ) {
              /* Skip this empty rectangle. */
              continue;
            }
          if(yuvFormat) {
              dstRects[m].left -= dstRects[m].left%2;
              dstRects[m].top -= dstRects[m].top%2;            
              dstRects[m].right -= dstRects[m].right%2;
              dstRects[m].bottom -= dstRects[m].bottom%2;
            }
          /* Advance to next rectangle. */
          m++;
        }

      /* Try next group if no rects. */
      if (m == 0) {
          hwcONERROR(hwcSTATUS_INVALID_ARGUMENT);
        }

        
      for ( int i = 0; i < m ; i++ ) {
          switch (Src->transform) {
            case 0:
              RotateMode = stretch;
              Xoffset = dstRects[i].left;
              Yoffset = dstRects[i].top;
              WidthAct = dstRects[i].right - dstRects[i].left ;
              HeightAct = dstRects[i].bottom - dstRects[i].top ;
              /* calculate srcRects,dstRect is virtual rect,dstRects[i] is actual rect,
                 hfactor and vfactor are scale factor.
              */
              srcRects[i].left   = srcRect.left - (int)((dstRect.left   - dstRects[i].left)   * hfactor);
              srcRects[i].top    = srcRect.top - (int)((dstRect.top    - dstRects[i].top)    * vfactor);
              srcRects[i].right  = srcRect.right - (int)((dstRect.right  - dstRects[i].right)  * hfactor);
              srcRects[i].bottom = srcRect.bottom - (int)((dstRect.bottom - dstRects[i].bottom) * vfactor);

              break;
            case HWC_TRANSFORM_FLIP_H:
              RotateMode      = 2;
              Xoffset = dstRects[i].left;
              Yoffset = dstRects[i].top;
              WidthAct = dstRects[i].right - dstRects[i].left ;
              HeightAct = dstRects[i].bottom - dstRects[i].top ;
              srcRects[i].left   = srcWidth - (srcRect.right - (int)((dstRect.right  - dstRects[i].right)  * hfactor));
              srcRects[i].top    = srcRect.top - (int)((dstRect.top    - dstRects[i].top)    * vfactor);
              srcRects[i].right  = srcWidth - (srcRect.left - (int)((dstRect.left   - dstRects[i].left)   * hfactor));
              srcRects[i].bottom = srcRect.bottom - (int)((dstRect.bottom - dstRects[i].bottom) * vfactor);

              break;
            case HWC_TRANSFORM_FLIP_V:
              RotateMode      = 3;
              Xoffset = dstRects[i].left;
              Yoffset = dstRects[i].top;
              WidthAct = dstRects[i].right - dstRects[i].left ;
              HeightAct = dstRects[i].bottom - dstRects[i].top ;
              srcRects[i].left   = srcRect.left - (int)((dstRect.left   - dstRects[i].left)   * hfactor);
              srcRects[i].top    = srcHeight - (srcRect.bottom - (int)((dstRect.bottom - dstRects[i].bottom) * vfactor));
              srcRects[i].right  = srcRect.right - (int)((dstRect.right  - dstRects[i].right)  * hfactor);
              srcRects[i].bottom = srcHeight - (srcRect.top - (int)((dstRect.top    - dstRects[i].top)    * vfactor));
              break;

            case HWC_TRANSFORM_ROT_90:
              RotateMode      = 1;
              Rotation    = 90;
              Xoffset = dstRects[i].right - 1;
              Yoffset = dstRects[i].top ;
              WidthAct = dstRects[i].bottom - dstRects[i].top ;
              HeightAct = dstRects[i].right - dstRects[i].left ;
              srcRects[i].left   = srcRect.left + (int)((dstRects[i].top -  dstRect.top)   * hfactor);
              srcRects[i].top    =  srcRect.top + (int)((dstRect.right - dstRects[i].right)   * vfactor);
              srcRects[i].right  = srcRects[i].left + (int)((dstRects[i].bottom - dstRects[i].top) * hfactor);
              srcRects[i].bottom = srcRects[i].top + (int)((dstRects[i].right  - dstRects[i].left) * vfactor);
              break;

            case HWC_TRANSFORM_ROT_180:
              RotateMode      = 1;
              Rotation    = 180;
              Xoffset = dstRects[i].right - 1;
              Yoffset = dstRects[i].bottom - 1;
              WidthAct = dstRects[i].right - dstRects[i].left;
              HeightAct = dstRects[i].bottom - dstRects[i].top;
              //srcRects[i].left   = srcRect.left +  (srcRect.right - srcRect.left)
              //- ((dstRects[i].right - dstRects[i].left) * hfactor)
              //+ ((dstRect.left   - dstRects[i].left)   * hfactor);
              srcRects[i].left   = srcRect.left + (int)((dstRect.right - dstRects[i].right)   * hfactor);
              srcRects[i].top    =  srcRect.top + (int)((dstRect.bottom - dstRects[i].bottom)   * vfactor);
              srcRects[i].right  = srcRects[i].left + (int)((dstRects[i].right  - dstRects[i].left) * hfactor);
              srcRects[i].bottom = srcRects[i].top + (int)((dstRects[i].bottom - dstRects[i].top) * vfactor);
              break;

            case HWC_TRANSFORM_ROT_270:
                
              RotateMode      = 1;
              Rotation        = 270;
              Xoffset = dstRects[i].left;
              Yoffset = dstRects[i].bottom - 1;
              WidthAct = dstRects[i].bottom - dstRects[i].top ;
              HeightAct = dstRects[i].right - dstRects[i].left ;
              //srcRects[i].left   = srcRect.top +  (srcRect.right - srcRect.left)
              //- ((dstRects[i].bottom - dstRects[i].top) * vfactor)
              //+ ((dstRect.top    - dstRects[i].top)    * vfactor);
              srcRects[i].left   = srcRect.left + (int)((dstRect.bottom - dstRects[i].bottom )   * hfactor);
              srcRects[i].top    =  srcRect.top + (int)((dstRects[i].left - dstRect.left )   * vfactor);
              srcRects[i].right  = srcRects[i].left + (int)((dstRects[i].bottom - dstRects[i].top) * hfactor);
              srcRects[i].bottom = srcRects[i].top + (int)((dstRects[i].right  - dstRects[i].left) * vfactor);
              break;
            default:
              hwcONERROR(hwcSTATUS_INVALID_ARGUMENT);
              break;
            }

          srcRects[i].left = hwcMAX(srcRects[i].left,0);
          srcRects[i].top = hwcMAX(srcRects[i].top,0); 
          srcRects[i].right =  hwcMIN(srcRects[i].right,srcRect.right);
          srcRects[i].bottom =  hwcMIN(srcRects[i].bottom,srcRect.bottom);

          if (yuvFormat)
            {
              srcRects[i].left -=   srcRects[i].left % 2;
              srcRects[i].top  -=  srcRects[i].top % 2;
              srcRects[i].right  -=  srcRects[i].right % 2;
              srcRects[i].bottom -=  srcRects[i].bottom % 2;
            }
          RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);
          RGA_set_src_act_info(&Rga_Request, srcRects[i].right -  srcRects[i].left, srcRects[i].bottom - srcRects[i].top,  srcRects[i].left, srcRects[i].top);
          //@@@@RGA_set_dst_act_info(&Rga_Request, WidthAct, HeightAct, Xoffset, Yoffset);
          RGA_set_dst_act_info(&Rga_Request, WidthAct, HeightAct, Xoffset, Yoffset + dstYOffset);//@@@@

          srcMmuType = srchnd->type ? 1 : 0;
          dstMmuType = DstHandle->type ? 1 : 0; //@@@@Context->membk_type[Context->membk_index] ? 1 : 0;
          if (srcMmuType || dstMmuType) {
              RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
              Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (dstMmuType << 10) | (srcMmuType << 8);
            }
          retv = ioctl(Context->engine_fd, RGA_BLIT_ASYNC, &Rga_Request);
          if( retv != 0) {
              LOGE("RGA ASYNC err=%d,name=%s",retv, Src->LayerName);
            }
        }
  } //while ( n < Region->numRects);
#endif

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdlib.h>
#include <sys/mman.h>

#include  <stdio.h>
#include  <time.h>

#define ALOG(fmt,...) printf(fmt "\n",__VA_ARGS__)
static int sina_table[360] =
{
    0,   1144,   2287,   3430,   4572,   5712,   6850,   7987,   9121,  10252,
    11380,  12505,  13626,  14742,  15855,  16962,  18064,  19161,  20252,  21336,
    22415,  23486,  24550,  25607,  26656,  27697,  28729,  29753,  30767,  31772,
    32768,  33754,  34729,  35693,  36647,  37590,  38521,  39441,  40348,  41243,
    42126,  42995,  43852,  44695,  45525,  46341,  47143,  47930,  48703,  49461,
    50203,  50931,  51643,  52339,  53020,  53684,  54332,  54963,  55578,  56175,
    56756,  57319,  57865,  58393,  58903,  59396,  59870,  60326,  60764,  61183,
    61584,  61966,  62328,  62672,  62997,  63303,  63589,  63856,  64104,  64332,
    64540,  64729,  64898,  65048,  65177,  65287,  65376,  65446,  65496,  65526,
    65536,  65526,  65496,  65446,  65376,  65287,  65177,  65048,  64898,  64729,
    64540,  64332,  64104,  63856,  63589,  63303,  62997,  62672,  62328,  61966,
    61584,  61183,  60764,  60326,  59870,  59396,  58903,  58393,  57865,  57319,
    56756,  56175,  55578,  54963,  54332,  53684,  53020,  52339,  51643,  50931,
    50203,  49461,  48703,  47930,  47143,  46341,  45525,  44695,  43852,  42995,
    42126,  41243,  40348,  39441,  38521,  37590,  36647,  35693,  34729,  33754,
    32768,  31772,  30767,  29753,  28729,  27697,  26656,  25607,  24550,  23486,
    22415,  21336,  20252,  19161,  18064,  16962,  15855,  14742,  13626,  12505,
    11380,  10252,   9121,   7987,   6850,   5712,   4572,   3430,   2287,   1144,
    0,  -1144,  -2287,  -3430,  -4572,  -5712,  -6850,  -7987,  -9121, -10252,
    -11380, -12505, -13626, -14742, -15855, -16962, -18064, -19161, -20252, -21336,
    -22415, -23486, -24550, -25607, -26656, -27697, -28729, -29753, -30767, -31772,
    -32768, -33754, -34729, -35693, -36647, -37590, -38521, -39441, -40348, -41243,
    -42126, -42995, -43852, -44695, -45525, -46341, -47143, -47930, -48703, -49461,
    -50203, -50931, -51643, -52339, -53020, -53684, -54332, -54963, -55578, -56175,
    -56756, -57319, -57865, -58393, -58903, -59396, -59870, -60326, -60764, -61183,
    -61584, -61966, -62328, -62672, -62997, -63303, -63589, -63856, -64104, -64332,
    -64540, -64729, -64898, -65048, -65177, -65287, -65376, -65446, -65496, -65526,
    -65536, -65526, -65496, -65446, -65376, -65287, -65177, -65048, -64898, -64729,
    -64540, -64332, -64104, -63856, -63589, -63303, -62997, -62672, -62328, -61966,
    -61584, -61183, -60764, -60326, -59870, -59396, -58903, -58393, -57865, -57319,
    -56756, -56175, -55578, -54963, -54332, -53684, -53020, -52339, -51643, -50931,
    -50203, -49461, -48703, -47930, -47143, -46341, -45525, -44695, -43852, -42995,
    -42126, -41243, -40348, -39441, -38521, -37590, -36647, -35693, -34729, -33754,
    -32768, -31772, -30767, -29753, -28729, -27697, -26656, -25607, -24550, -23486,
    -22415, -21336, -20252, -19161, -18064, -16962, -15855, -14742, -13626, -12505,
    -11380, -10252, -9121,   -7987,  -6850,  -5712,  -4572,  -3430,  -2287,  -1144
};


static int cosa_table[360] =
{

    65536,  65526,  65496,  65446,  65376,  65287,  65177,  65048,  64898,  64729,
    64540,  64332,  64104,  63856,  63589,  63303,  62997,  62672,  62328,  61966,
    61584,  61183,  60764,  60326,  59870,  59396,  58903,  58393,  57865,  57319,
    56756,  56175,  55578,  54963,  54332,  53684,  53020,  52339,  51643,  50931,
    50203,  49461,  48703,  47930,  47143,  46341,  45525,  44695,  43852,  42995,
    42126,  41243,  40348,  39441,  38521,  37590,  36647,  35693,  34729,  33754,
    32768,  31772,  30767,  29753,  28729,  27697,  26656,  25607,  24550,  23486,
    22415,  21336,  20252,  19161,  18064,  16962,  15855,  14742,  13626,  12505,
    11380,  10252,   9121,   7987,   6850,   5712,   4572,   3430,   2287,   1144,
    0,  -1144,  -2287,  -3430,  -4572,  -5712,  -6850,  -7987,  -9121, -10252,
    -11380, -12505, -13626, -14742, -15855, -16962, -18064, -19161, -20252, -21336,
    -22415, -23486, -24550, -25607, -26656, -27697, -28729, -29753, -30767, -31772,
    -32768, -33754, -34729, -35693, -36647, -37590, -38521, -39441, -40348, -41243,
    -42126, -42995, -43852, -44695, -45525, -46341, -47143, -47930, -48703, -49461,
    -50203, -50931, -51643, -52339, -53020, -53684, -54332, -54963, -55578, -56175,
    -56756, -57319, -57865, -58393, -58903, -59396, -59870, -60326, -60764, -61183,
    -61584, -61966, -62328, -62672, -62997, -63303, -63589, -63856, -64104, -64332,
    -64540, -64729, -64898, -65048, -65177, -65287, -65376, -65446, -65496, -65526,
    -65536, -65526, -65496, -65446, -65376, -65287, -65177, -65048, -64898, -64729,
    -64540, -64332, -64104, -63856, -63589, -63303, -62997, -62672, -62328, -61966,
    -61584, -61183, -60764, -60326, -59870, -59396, -58903, -58393, -57865, -57319,
    -56756, -56175, -55578, -54963, -54332, -53684, -53020, -52339, -51643, -50931,
    -50203, -49461, -48703, -47930, -47143, -46341, -45525, -44695, -43852, -42995,
    -42126, -41243, -40348, -39441, -38521, -37590, -36647, -35693, -34729, -33754,
    -32768, -31772, -30767, -29753, -28729, -27697, -26656, -25607, -24550, -23486,
    -22415, -21336, -20252, -19161, -18064, -16962, -15855, -14742, -13626, -12505,
    -11380, -10252,  -9121,  -7987,  -6850,  -5712,  -4572,  -3430,  -2287,  -1144,
    0,   1144,   2287,   3430,   4572,   5712,   6850,   7987,   9121,  10252,
    11380,  12505,  13626,  14742,  15855,  16962,  18064,  19161,  20252,  21336,
    22415,  23486,  24550,  25607,  26656,  27697,  28729,  29753,  30767,  31772,
    32768,  33754,  34729,  35693,  36647,  37590,  38521,  39441,  40348,  41243,
    42126,  42995,  43852,  44695,  45525,  46341,  47143,  47930,  48703,  49461,
    50203,  50931,  51643,  52339,  53020,  53684,  54332,  54963,  55578,  56175,
    56756,  57319,  57865,  58393,  58903,  59396,  59870,  60326,  60764,  61183,
    61584,  61966,  62328,  62672,  62997,  63303,  63589,  63856,  64104,  64332,
    64540,  64729,  64898,  65048,  65177,  65287,  65376,  65446,  65496,  65526
};


#define RGA_BLIT_SYNC	0x5017
#define RGA_BLIT_ASYNC  0x5018
#define RGA_FLUSH       0x5019
#define RGA_GET_RESULT  0x501a
#define RGA_GET_VERSION 0x501b
#define RGA_REG_CTRL_LEN    0x8    /* 8  */
#define RGA_REG_CMD_LEN     0x1c   /* 28 */
#define RGA_CMD_BUF_SIZE    0x700  /* 16*28*4 */

enum {
    HAL_TRANSFORM_FLIP_H    = 0x01,
    /* flip source image vertically (around the horizontal axis)*/
    HAL_TRANSFORM_FLIP_V    = 0x02,
    /* rotate source image 90 degrees clockwise */
    HAL_TRANSFORM_ROT_90    = 0x04,
    /* rotate source image 180 degrees */
    HAL_TRANSFORM_ROT_180   = 0x03,
    /* rotate source image 270 degrees clockwise */
    HAL_TRANSFORM_ROT_270   = 0x07,
    /* don't use. see system/window.h */
    HAL_TRANSFORM_RESERVED  = 0x08,
};

typedef struct hwc_rect {
    int left;
    int top;
    int right;
    int bottom;
} hwc_rect_t;

/* RGA process mode enum */
//@@@@  (rga_req.render_mode )
enum
{    
    bitblt_mode               = 0x0,
    color_palette_mode        = 0x1,
    color_fill_mode           = 0x2,
    line_point_drawing_mode   = 0x3,
    blur_sharp_filter_mode    = 0x4,
    pre_scaling_mode          = 0x5,
    update_palette_table_mode = 0x6,
    update_patten_buff_mode   = 0x7,
};


enum
{
    rop_enable_mask          = 0x2,
    dither_enable_mask       = 0x8,
    fading_enable_mask       = 0x10,
    PD_enbale_mask           = 0x20,
};

enum
{
    yuv2rgb_mode0            = 0x0,     /* BT.601 MPEG */
    yuv2rgb_mode1            = 0x1,     /* BT.601 JPEG */
    yuv2rgb_mode2            = 0x2,     /* BT.709      */
};


/* RGA rotate mode */
enum 
{
    rotate_mode0             = 0x0,     /* no rotate */
    rotate_mode1             = 0x1,     /* rotate    */
    rotate_mode2             = 0x2,     /* x_mirror  */
    rotate_mode3             = 0x3,     /* y_mirror  */
};

enum
{
    color_palette_mode0      = 0x0,     /* 1K */
    color_palette_mode1      = 0x1,     /* 2K */
    color_palette_mode2      = 0x2,     /* 4K */
    color_palette_mode3      = 0x3,     /* 8K */
};

enum 
{
    BB_BYPASS   = 0x0,     /* no rotate */
    BB_ROTATE   = 0x1,     /* rotate    */
    BB_X_MIRROR = 0x2,     /* x_mirror  */
    BB_Y_MIRROR = 0x3      /* y_mirror  */
};

enum 
{
    nearby   = 0x0,     /* no rotate */
    bilinear = 0x1,     /* rotate    */
    bicubic  = 0x2,     /* x_mirror  */
};


/*
//          Alpha    Red     Green   Blue  
{  4, 32, {{32,24,   8, 0,  16, 8,  24,16 }}, GGL_RGBA },   // RK_FORMAT_RGBA_8888    
{  4, 24, {{ 0, 0,   8, 0,  16, 8,  24,16 }}, GGL_RGB  },   // RK_FORMAT_RGBX_8888    
{  3, 24, {{ 0, 0,   8, 0,  16, 8,  24,16 }}, GGL_RGB  },   // RK_FORMAT_RGB_888
{  4, 32, {{32,24,  24,16,  16, 8,   8, 0 }}, GGL_BGRA },   // RK_FORMAT_BGRA_8888
{  2, 16, {{ 0, 0,  16,11,  11, 5,   5, 0 }}, GGL_RGB  },   // RK_FORMAT_RGB_565        
{  2, 16, {{ 1, 0,  16,11,  11, 6,   6, 1 }}, GGL_RGBA },   // RK_FORMAT_RGBA_5551    
{  2, 16, {{ 4, 0,  16,12,  12, 8,   8, 4 }}, GGL_RGBA },   // RK_FORMAT_RGBA_4444
{  3, 24, {{ 0, 0,  24,16,  16, 8,   8, 0 }}, GGL_BGR  },   // RK_FORMAT_BGB_888

*/
typedef enum _Rga_SURF_FORMAT
{
	RK_FORMAT_RGBA_8888    = 0x0,
    RK_FORMAT_RGBX_8888    = 0x1,
    RK_FORMAT_RGB_888      = 0x2,
    RK_FORMAT_BGRA_8888    = 0x3,
    RK_FORMAT_RGB_565      = 0x4,
    RK_FORMAT_RGBA_5551    = 0x5,
    RK_FORMAT_RGBA_4444    = 0x6,
    RK_FORMAT_BGR_888      = 0x7,
    
    RK_FORMAT_YCbCr_422_SP = 0x8,    
    RK_FORMAT_YCbCr_422_P  = 0x9,    
    RK_FORMAT_YCbCr_420_SP = 0xa,    
    RK_FORMAT_YCbCr_420_P  = 0xb,

    RK_FORMAT_YCrCb_422_SP = 0xc,    
    RK_FORMAT_YCrCb_422_P  = 0xd,    
    RK_FORMAT_YCrCb_420_SP = 0xe,    
    RK_FORMAT_YCrCb_420_P  = 0xf,
    
    RK_FORMAT_BPP1         = 0x10,
    RK_FORMAT_BPP2         = 0x11,
    RK_FORMAT_BPP4         = 0x12,
    RK_FORMAT_BPP8         = 0x13,
    RK_FORMAT_YCbCr_420_SP_10B = 0x20,
    RK_FORMAT_YCrCb_420_SP_10B = 0x21,
    RK_FORMAT_UNKNOWN       = 0x100, 
}RgaSURF_FORMAT;
    
    
typedef struct rga_img_info_t
{
    unsigned int yrgb_addr;      /* yrgb    mem addr         */
    unsigned int uv_addr;        /* cb/cr   mem addr         */
    unsigned int v_addr;         /* cr      mem addr         */
    unsigned int format;         //definition by RK_FORMAT
    unsigned short act_w;
    unsigned short act_h;
    unsigned short x_offset;
    unsigned short y_offset;
    
    unsigned short vir_w;
    unsigned short vir_h;
    
    unsigned short endian_mode; //for BPP
    unsigned short alpha_swap;
}
rga_img_info_t;


typedef struct mdp_img_act
{
    unsigned short w;         // width
    unsigned short h;         // height
    short x_off;     // x offset for the vir
    short y_off;     // y offset for the vir
}
mdp_img_act;



typedef struct RANGE
{
    unsigned short min;
    unsigned short max;
}
RANGE;

typedef struct POINT
{
    unsigned short x;
    unsigned short y;
}
POINT;

typedef struct RECT
{
    unsigned short xmin;
    unsigned short xmax; // width - 1
    unsigned short ymin; 
    unsigned short ymax; // height - 1 
} RECT;

typedef struct RGB
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char res;
}RGB;


typedef struct MMU
{
    unsigned char mmu_en;
    unsigned int base_addr;
    unsigned int mmu_flag;     /* [0] mmu enable [1] src_flush [2] dst_flush [3] CMD_flush [4~5] page size*/
} MMU;




typedef struct COLOR_FILL
{
    short gr_x_a;
    short gr_y_a;
    short gr_x_b;
    short gr_y_b;
    short gr_x_g;
    short gr_y_g;
    short gr_x_r;
    short gr_y_r;

    //u8  cp_gr_saturation;
}
COLOR_FILL;

typedef struct FADING
{
    unsigned char b;
    unsigned char g;
    unsigned char r;
    unsigned char res;
}
FADING;


typedef struct line_draw_t
{
    POINT start_point;              /* LineDraw_start_point                */
    POINT end_point;                /* LineDraw_end_point                  */
    unsigned int   color;               /* LineDraw_color                      */
    unsigned int   flag;                /* (enum) LineDrawing mode sel         */
    unsigned int   line_width;          /* range 1~16 */
}
line_draw_t;



struct rga_req { 
    unsigned char render_mode;            /* (enum) process mode sel */
    
    rga_img_info_t src;                   /* src image info */
    rga_img_info_t dst;                   /* dst image info */
    rga_img_info_t pat;             /* patten image info */

    unsigned int rop_mask_addr;         /* rop4 mask addr */
    unsigned int LUT_addr;              /* LUT addr */
    RECT clip;                      /* dst clip window default value is dst_vir */
                                    /* value from [0, w-1] / [0, h-1]*/
        
    int sina;                   /* dst angle  default value 0  16.16 scan from table */
    int cosa;                   /* dst angle  default value 0  16.16 scan from table */        

    unsigned short alpha_rop_flag;        /* alpha rop process flag           */
                                    /* ([0] = 1 alpha_rop_enable)       */
                                    /* ([1] = 1 rop enable)             */                                                                                                                
                                    /* ([2] = 1 fading_enable)          */
                                    /* ([3] = 1 PD_enable)              */
                                    /* ([4] = 1 alpha cal_mode_sel)     */
                                    /* ([5] = 1 dither_enable)          */
                                    /* ([6] = 1 gradient fill mode sel) */
                                    /* ([7] = 1 AA_enable)              */

    unsigned char  scale_mode;            /* 0 nearst / 1 bilnear / 2 bicubic */                             
                            
    unsigned int color_key_max;         /* color key max */
    unsigned int color_key_min;         /* color key min */     

    unsigned int fg_color;              /* foreground color */
    unsigned int bg_color;              /* background color */

    COLOR_FILL gr_color;            /* color fill use gradient */
    
    line_draw_t line_draw_info;
    
    FADING fading;
                              
    unsigned char PD_mode;                /* porter duff alpha mode sel */
    
    unsigned char alpha_global_value;     /* global alpha value */
     
    unsigned short rop_code;              /* rop2/3/4 code  scan from rop code table*/
    
    unsigned char bsfilter_flag;          /* [2] 0 blur 1 sharp / [1:0] filter_type*/
    
    unsigned char palette_mode;           /* (enum) color palatte  0/1bpp, 1/2bpp 2/4bpp 3/8bpp*/

    unsigned char yuv2rgb_mode;           /* (enum) BT.601 MPEG / BT.601 JPEG / BT.709  */ 
    
    unsigned char endian_mode;            /* 0/big endian 1/little endian*/

    unsigned char rotate_mode;            /* (enum) rotate mode  */
                                    /* 0x0,     no rotate  */
                                    /* 0x1,     rotate     */
                                    /* 0x2,     x_mirror   */
                                    /* 0x3,     y_mirror   */

    unsigned char color_fill_mode;        /* 0 solid color / 1 patten color */
                                    
    MMU mmu_info;                   /* mmu information */

    unsigned char  alpha_rop_mode;        /* ([0~1] alpha mode)       */
                                    /* ([2~3] rop   mode)       */
                                    /* ([4]   zero  mode en)    */
                                    /* ([5]   dst   alpha mode) */

    unsigned char  src_trans_mode;

    unsigned char CMD_fin_int_enable;                        

    /* completion is reported through a callback */
	void (*complete)(int retval);
};


int
RGA_set_dst_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		);

int
RGA_set_dst_vir_info(
		struct rga_req *msg,
		unsigned int   yrgb_addr,   /* yrgb_addr   */
		unsigned int   uv_addr,     /* uv_addr     */
		unsigned int   v_addr,      /* v_addr      */
		unsigned int   vir_w,       /* vir width   */
		unsigned int   vir_h,       /* vir height  */
		RECT           *clip,        /* clip window */
		unsigned char  format,      /* format      */
		unsigned char  a_swap_en
		);

int 
RGA_set_pat_info(
    struct rga_req *msg,
    unsigned int width,
    unsigned int height,
    unsigned int x_off,
    unsigned int y_off,
    unsigned int pat_format    
    );

int
RGA_set_rop_mask_info(
		struct rga_req *msg,
		unsigned int rop_mask_addr,
		unsigned int rop_mask_endian_mode
		);
   
int RGA_set_alpha_en_info(
		struct rga_req *msg,
		unsigned int  alpha_cal_mode,    /* 0:alpha' = alpha + (alpha>>7) | alpha' = alpha */
		unsigned int  alpha_mode,        /* 0 global alpha / 1 per pixel alpha / 2 mix mode */
		unsigned int  global_a_value,
		unsigned int  PD_en,             /* porter duff alpha mode en */ 
		unsigned int  PD_mode, 
		unsigned int  dst_alpha_en );    /* use dst alpha  */

int
RGA_set_rop_en_info(
		struct rga_req *msg,
		unsigned int ROP_mode,
		unsigned int ROP_code,
		unsigned int color_mode,
		unsigned int solid_color
		);

 
int
RGA_set_fading_en_info(
		struct rga_req *msg,
		unsigned char r,
		unsigned char g,
		unsigned char b
		);

int
RGA_set_src_trans_mode_info(
		struct rga_req *msg,
		unsigned char trans_mode,
		unsigned char a_en,
		unsigned char b_en,
		unsigned char g_en,
		unsigned char r_en,
		unsigned char color_key_min,
		unsigned char color_key_max,
		unsigned char zero_mode_en
		);


int
RGA_set_bitblt_mode(
		struct rga_req *msg,
		unsigned char scale_mode,    // 0/near  1/bilnear  2/bicubic  
		unsigned char rotate_mode,   // 0/copy 1/rotate_scale 2/x_mirror 3/y_mirror 
		unsigned int  angle,         // rotate angle     
		unsigned int  dither_en,     // dither en flag   
		unsigned int  AA_en,         // AA flag          
		unsigned int  yuv2rgb_mode
		)
{
    unsigned int alpha_mode;
    msg->render_mode = bitblt_mode;

    //@@@@if(((msg->src.act_w >> 1) > msg->dst.act_w) || ((msg->src.act_h >> 1) > msg->dst.act_h))
    //@@@@  return -1;
    
    msg->scale_mode = scale_mode;
    msg->rotate_mode = rotate_mode;
    
    //@@@@
    msg->sina = sina_table[angle];
    msg->cosa = cosa_table[angle];

    msg->yuv2rgb_mode = yuv2rgb_mode;

    msg->alpha_rop_flag |= ((AA_en << 7) & 0x80);

    alpha_mode = msg->alpha_rop_mode & 3;
    if(rotate_mode == BB_ROTATE)
    {
      if (AA_en == 1/*ENABLE*/) 
        {   
            if ((msg->alpha_rop_flag & 0x3) == 0x1)
            {
                if (alpha_mode == 0)
                {
                msg->alpha_rop_mode = 0x2;            
                }
                else if (alpha_mode == 1)
                {
                    msg->alpha_rop_mode = 0x1;
                }
            }
            else
            {
                msg->alpha_rop_flag |= 1;
                msg->alpha_rop_mode = 1;
            }                        
        }        
    }
   
    if (msg->src_trans_mode)
        msg->scale_mode = 0;

    msg->alpha_rop_flag |= (dither_en << 5);
    
    return 0;
}


int
RGA_set_color_palette_mode(
		struct rga_req *msg,
		unsigned char  palette_mode,        /* 1bpp/2bpp/4bpp/8bpp */
		unsigned char  endian_mode,         /* src endian mode sel */
		unsigned int  bpp1_0_color,         /* BPP1 = 0 */
		unsigned int  bpp1_1_color          /* BPP1 = 1 */
		);


int
RGA_set_color_fill_mode(
		struct rga_req *msg,
		COLOR_FILL  *gr_color,                    /* gradient color part         */
		unsigned char  gr_satur_mode,            /* saturation mode             */
    	unsigned char  cf_mode,                  /* patten fill or solid fill   */
		unsigned int color,                      /* solid color                 */
		unsigned short pat_width,                /* pattern width               */
		unsigned short pat_height,               /* pattern height              */   
		unsigned char pat_x_off,                 /* pattern x offset            */
		unsigned char pat_y_off,                 /* pattern y offset            */
		unsigned char aa_en                      /* alpha en                    */
		);


int
RGA_set_line_point_drawing_mode(
		struct rga_req *msg,
		POINT sp,                     /* start point              */
		POINT ep,                     /* end   point              */
		unsigned int color,           /* line point drawing color */
		unsigned int line_width,      /* line width               */
		unsigned char AA_en,          /* AA en                    */
		unsigned char last_point_en   /* last point en            */
		);


int
RGA_set_blur_sharp_filter_mode(
		struct rga_req *msg,
		unsigned char filter_mode,   /* blur/sharpness   */
		unsigned char filter_type,   /* filter intensity */
		unsigned char dither_en      /* dither_en flag   */
		);

int
RGA_set_pre_scaling_mode(
		struct rga_req *msg,
		unsigned char dither_en
		);

int
RGA_update_palette_table_mode(
		struct rga_req *msg,
		unsigned int LUT_addr,      /* LUT table addr      */
		unsigned int palette_mode   /* 1bpp/2bpp/4bpp/8bpp */
		);

int
RGA_set_update_patten_buff_mode(
		struct rga_req *msg,
		unsigned int pat_addr, /* patten addr    */
		unsigned int w,        /* patten width   */
		unsigned int h,        /* patten height  */
		unsigned int format    /* patten format  */
		);

int
RGA_set_mmu_info(
		struct rga_req *msg,
		unsigned char  mmu_en,
		unsigned char  src_flush,
		unsigned char  dst_flush,
		unsigned char  cmd_flush,
		unsigned int base_addr,
		unsigned char  page_size
		);

void rga_set_fds_offsets(
        struct rga_req *rga_request,
        unsigned short src_fd,
        unsigned short dst_fd,
        unsigned int src_offset, 
        unsigned int dst_offset);

int
RGA_set_src_fence_flag(
    struct rga_req *msg,
    int acq_fence,
    int src_flag
);


int
RGA_set_dst_fence_flag(
    struct rga_req *msg,
    int dst_flag
);

int
RGA_get_dst_fence(
    struct rga_req *msg
);

//#define PAGE_SIZE 4096
inline size_t round_up_to_page_size(size_t x)
{
	return (x + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
}


void rga_set_fds_offsets(struct rga_req *rga_request,
			 uint16_t src_fd, uint16_t dst_fd,
			 uint32_t src_offset, uint32_t dst_offset)
{
       rga_request->line_draw_info.color = src_fd | (dst_fd << 16);
       rga_request->line_draw_info.flag = src_offset;
       rga_request->line_draw_info.line_width = dst_offset;
}


int
RGA_set_mmu_info(
		struct rga_req *msg,
		unsigned char  mmu_en,
		unsigned char  src_flush,
		unsigned char  dst_flush,
		unsigned char  cmd_flush,
		unsigned int base_addr,
		unsigned char  page_size
		)
{
    msg->mmu_info.mmu_en    = mmu_en;
    msg->mmu_info.base_addr = base_addr;
    msg->mmu_info.mmu_flag  = ((page_size & 0x3) << 4) |
                              ((cmd_flush & 0x1) << 3) |
                              ((dst_flush & 0x1) << 2) | 
                              ((src_flush & 0x1) << 1) | mmu_en;
    return 1;
}

int
RGA_set_color_fill_mode(
		struct rga_req *msg,
		COLOR_FILL  *gr_color,              /* gradient color part         */
		unsigned char  gr_satur_mode,            /* saturation mode             */
    	unsigned char  cf_mode,                  /* patten fill or solid fill   */
		unsigned int color,                    /* solid color                 */
		unsigned short pat_width,                /* pattern width               */
		unsigned short pat_height,               /* pattern height              */   
		unsigned char pat_x_off,                 /* pattern x offset            */
		unsigned char pat_y_off,                 /* pattern y offset            */
		unsigned char aa_en                      /* alpha en                    */
		)
{
    msg->render_mode = color_fill_mode;

    msg->gr_color.gr_x_a = ((int)(gr_color->gr_x_a * 256.0))& 0xffff;
    msg->gr_color.gr_x_b = ((int)(gr_color->gr_x_b * 256.0))& 0xffff;
    msg->gr_color.gr_x_g = ((int)(gr_color->gr_x_g * 256.0))& 0xffff;
    msg->gr_color.gr_x_r = ((int)(gr_color->gr_x_r * 256.0))& 0xffff;

    msg->gr_color.gr_y_a = ((int)(gr_color->gr_y_a * 256.0))& 0xffff;
    msg->gr_color.gr_y_b = ((int)(gr_color->gr_y_b * 256.0))& 0xffff;
    msg->gr_color.gr_y_g = ((int)(gr_color->gr_y_g * 256.0))& 0xffff;
    msg->gr_color.gr_y_r = ((int)(gr_color->gr_y_r * 256.0))& 0xffff;

    msg->color_fill_mode = cf_mode;
    
    msg->pat.act_w = pat_width;
    msg->pat.act_h = pat_height;

    msg->pat.x_offset = pat_x_off;
    msg->pat.y_offset = pat_y_off;

    msg->fg_color = color;

    msg->alpha_rop_flag |= ((gr_satur_mode & 1) << 6);
    
    if(aa_en)
    {
    	msg->alpha_rop_flag |= 0x1;
    	msg->alpha_rop_mode  = 1;
    }
    return 1;
}

int
RGA_set_dst_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		)
{
    req->dst.act_w = width;
    req->dst.act_h = height;
    req->dst.x_offset = x_off;
    req->dst.y_offset = y_off;
    
    return 1;
}

int
RGA_set_src_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		)
{
    req->src.act_w = width;
    req->src.act_h = height;
    req->src.x_offset = x_off;
    req->src.y_offset = y_off;
    
    return 1;
}

int
RGA_set_dst_vir_info(
		struct rga_req *msg,
		unsigned int   yrgb_addr,   /* yrgb_addr   */
		unsigned int   uv_addr,     /* uv_addr     */
		unsigned int   v_addr,      /* v_addr      */
		unsigned int   vir_w,       /* vir width   */
		unsigned int   vir_h,       /* vir height  */
		RECT           *clip,        /* clip window */
		unsigned char  format,      /* format      */
		unsigned char  a_swap_en
		)
{
    msg->dst.yrgb_addr = yrgb_addr;
    msg->dst.uv_addr  = uv_addr;
    msg->dst.v_addr   = v_addr;
    msg->dst.vir_w = vir_w;
    msg->dst.vir_h = vir_h;
    msg->dst.format = format;

    msg->clip.xmin = clip->xmin;
    msg->clip.xmax = clip->xmax;
    msg->clip.ymin = clip->ymin;
    msg->clip.ymax = clip->ymax;
    
    msg->dst.alpha_swap |= (a_swap_en & 1);
    
    return 1;
}

int
RGA_set_src_vir_info(
		struct rga_req *req,
		unsigned int   yrgb_addr,       /* yrgb_addr  */
		unsigned int   uv_addr,         /* uv_addr    */
		unsigned int   v_addr,          /* v_addr     */
		unsigned int   vir_w,           /* vir width  */
		unsigned int   vir_h,           /* vir height */
		unsigned char  format,          /* format     */
		unsigned char  a_swap_en        /* only for 32bit RGB888 format */
		)
{
    req->src.yrgb_addr = yrgb_addr;
    req->src.uv_addr  = uv_addr;
    req->src.v_addr   = v_addr;
    req->src.vir_w = vir_w;
    req->src.vir_h = vir_h;
    req->src.format = format;
    req->src.alpha_swap |= (a_swap_en & 1);

    return 1;
}


void swap( int& v1, int& v2 )
{
  int   tmp = v1;
  v1 = v2;
  v2 = tmp;
}


//----------------------------------------------------------
// buffer size, etc..
//----------------------------------------------------------
#define PIXEL_FORMAT          RK_FORMAT_RGBA_8888
#define PIXEL_SIZE_IN_BYTES   4

#define SCREEN_WIDTH    1024
#define SCREEN_HEIGHT   600

#define SCREEN_STRIDE         SCREEN_WIDTH
#define NUM_BUFFER            3

//----------------------------------------------------------
// hwcBlit
//----------------------------------------------------------

// for test
struct g_ {
  int   srcLeft, srcTop, srcWidth, srcHeight;
} g;

typedef unsigned short  uint16_t;
void hwcBlit( int fdRGA,
              hwc_rect_t const& rectSrc,
              hwc_rect_t const& rectDst,
              int share_fdSrc,
              int share_fdDst,
              int srcHeightVir,
              int rotation )
{

  int        dstWidth  = rectDst.right - rectDst.left;
  int        dstHeight = rectDst.bottom - rectDst.top;
  int        dstStride = SCREEN_STRIDE;
  RgaSURF_FORMAT      dstFormat = RK_FORMAT_RGBA_8888;
  unsigned int        dstHeightVir = 600*3;


  int        srcWidth  = rectSrc.right - rectSrc.left;
  int        srcHeight = rectSrc.bottom - rectSrc.top;
  int        srcStride = SCREEN_STRIDE;
  RgaSURF_FORMAT      srcFormat = PIXEL_FORMAT;
  //unsigned int        srcHeightVir = SCREEN_HEIGHT*NUM_BUFFER;

  //unsigned int        yOffsetCommon = offset / (PIXEL_SIZE_IN_BYTES*SCREEN_STRIDE);



  //RECT                clip = { (uint16_t)rectDst.left, (uint16_t)(rectDst.right-1), (uint16_t)(rectDst.top + yOffsetCommon), (uint16_t)(rectDst.bottom-1 + yOffsetCommon ) };
  RECT                clip = { 0, (uint16_t)(dstStride-1), 0, (uint16_t)(dstHeightVir-1) };
    //unsigned short xmin;
    //unsigned short xmax; // width - 1
    //unsigned short ymin; 
    //unsigned short ymax; // height - 1 
  struct rga_req  Rga_Request;

  memset(&Rga_Request, 0x0, sizeof(Rga_Request));
  //RGA_set_src_vir_info(&Rga_Request, share_fdSrc, (intptr_t)vaddr,  0, srcStride, srcHeightVir, srcFormat, 0);
  //RGA_set_dst_vir_info(&Rga_Request, share_fdDst, (intptr_t)vaddr,  0, dstStride, dstHeightVir, &clip, dstFormat, 0);

  RGA_set_src_vir_info(&Rga_Request, share_fdSrc, 0,  0, srcStride, srcHeightVir, srcFormat, 0);
  RGA_set_dst_vir_info(&Rga_Request, share_fdDst, 0,  0, dstStride, dstHeightVir, &clip, dstFormat, 0);

  //(NG) RGA_set_src_vir_info(&Rga_Request, (intptr_t)vaddr, 0,  0, srcStride, srcHeightVir, srcFormat, 0);
  //(NG) RGA_set_dst_vir_info(&Rga_Request, (intptr_t)vaddr, 0,  0, dstStride, dstHeightVir, &clip, dstFormat, 0);
  rga_set_fds_offsets(&Rga_Request, 0, 0, 0, 0);


  // alpha( premult )
  #if 0
  //...pixel and plane alpha
  RGA_set_alpha_en_info(&Rga_Request, 1, 2, planeAlpha , 1, 9, 0);
  //...pixel alphaonly
  RGA_set_alpha_en_info(&Rga_Request, 1, 1, 0, 1, 3, 0);
  //...plane alpha only
  RGA_set_alpha_en_info(&Rga_Request, 1, 0, planeAlpha , 0, 0, 0);
  // alpha( coverage )
  //...pixel and plane alpha
  RGA_set_alpha_en_info(&Rga_Request, 1, 2, planeAlpha , 0, 0, 0);
  //...pixel alphaonly
  RGA_set_alpha_en_info(&Rga_Request, 1, 1, 0, 0, 0, 0);
  //...plane alpha only
  RGA_set_alpha_en_info(&Rga_Request, 1, 0, planeAlpha , 0, 0, 0);
  #endif
  
  // rotation
  int   dither_en = 0; // always 0 : android::bytesPerPixel(GPU_FORMAT) == android::bytesPerPixel(GPU_DST_FORMAT) ? 0 : 1;
  int   scale_mode = 0; // 0/near  1/bilnear  2/bicubic : 0(non if srcFormat == RK_FORMAT_RGBA_8888 )
  int   RotateMode = 1; // 0/copy 1/rotate_scale 2/x_mirror 3/y_mirror 
  int   Rotation = 0; 
  int   srcOffsetX = rectSrc.left, dstOffsetX = rectDst.left;
  int   srcOffsetY = rectSrc.top, dstOffsetY = rectDst.top;

  // for test
#if 1
  // It seems HAL_TRANSFORM_FLIP_H/V is not supprted, due to the driver implementation ( not to h/w spec ).
  switch (rotation) {
  case 0:
    break;
  case 1://HAL_TRANSFORM_FLIP_H:  // 1
    RotateMode = 2;
    //dstWidth *= 2;
    //srcOffsetX = srcStride - rectSrc.right ;
    //srcOffsetX = srcStride + rectSrc.right ;
    //srcOffsetX = srcStride - rectSrc.right + 4;
    //srcOffsetX = srcStride;
    srcOffsetX = srcStride - rectSrc.right;
    //dstOffsetX = rectDst.right - 100;
    //srcOffsetY += 1;
    //srcWidth = - srcWidth;
    break;
  case 2://HAL_TRANSFORM_FLIP_V:  // 2
    RotateMode = 3;
    srcOffsetY = srcHeightVir - rectSrc.bottom;
    break;
  case 4: //HAL_TRANSFORM_ROT_90:  // 4
    Rotation = 90;
    swap( dstWidth, dstHeight );
    //swap( srcWidth, srcHeight );
    dstOffsetX = rectDst.right -1;
    break;
  case 3: //HAL_TRANSFORM_ROT_180: // 3
    dstOffsetX = rectDst.right -1;
    dstOffsetY = rectDst.bottom -1;
    Rotation = 180;
    break;
  case 7://HAL_TRANSFORM_ROT_270: // 7
    Rotation = 270;
    swap( dstWidth, dstHeight );
    //swap( srcWidth, srcHeight );
    dstOffsetY = rectDst.bottom -1;
    break;
  default:;
    ALOG("@@@ invalid rotation=%d", rotation);
  }
#else
  // for test
  srcOffsetX = g.srcLeft;
  srcOffsetY = g.srcTop;
  srcWidth = g.srcWidth;
  srcHeight = g.srcHeight;
  RotateMode = 2;
#endif  // 0


  RGA_set_src_act_info(&Rga_Request, srcWidth, srcHeight, srcOffsetX, srcOffsetY);
  RGA_set_dst_act_info(&Rga_Request, dstWidth, dstHeight, dstOffsetX, dstOffsetY);
  RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);

  // for test
  //if (rotation == 1) {
  //  Rga_Request.render_mode = color_fill_mode;
  //}

  int srcMmuType = 1;
  int dstMmuType = 1;
  RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
  Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (dstMmuType << 10) | (srcMmuType << 8);

  //Rga_Request.src_trans_mode = 1;// for test
  if (ioctl(fdRGA, RGA_BLIT_ASYNC, &Rga_Request) != 0) {
      ALOG("%s(%d):  RGA_BLIT_ASYNC Failed", __FUNCTION__, __LINE__);
  }
  if ( ioctl(fdRGA, RGA_FLUSH, NULL) != 0 ) {
      ALOG("%s(%d):  RGA_FLUSH Failed", __FUNCTION__, __LINE__);
  }
}


//----------------------------------------------------------
// ion
//----------------------------------------------------------
typedef int ion_user_handle_t;
extern "C" {
int ion_open();
int ion_close(int fd);
int ion_alloc(int fd, size_t len, size_t align, unsigned int heap_mask,
              unsigned int flags, ion_user_handle_t *handle);
int ion_free(int fd, ion_user_handle_t handle);
int ion_map(int fd, ion_user_handle_t handle, size_t length, int prot,
            int flags, off_t offset, unsigned char **ptr, int *map_fd);
int ion_share(int fd, ion_user_handle_t handle, int *share_fd);
int ion_alloc_fd(int fd, size_t len, size_t align, unsigned int heap_mask,
                 unsigned int flags, int *handle_fd);
int ion_import(int fd, int share_fd, ion_user_handle_t *handle);
 int ion_sync_fd(int fd, int handle_fd);
int ion_get_phys(int fd, ion_user_handle_t handle, unsigned long *phys);
};
//----------------------------------------------------------
// main
//----------------------------------------------------------
int main(int argc, const char*argv[] )
{
  int       errLine = -1;
  int		    fdFB = 0;
  int const sizeFB = SCREEN_STRIDE * SCREEN_HEIGHT * PIXEL_SIZE_IN_BYTES;  // rgba

  int       index, rotation = 0;
  
  int*      pBuff; 
  size_t    sizeFBALL;
	void *    vaddr;


  // index <-- cmd line
  // for debug
#if 1
  if ( argc < 2 ) {
    printf("usage : %s <index(0..2)> [<rotation<0..3>]", argv[0] );
		errLine = __LINE__; goto ret;
  }
  index = atoi( argv[1] );
  if ( index < 0 || index > 2 ) {
		errLine = __LINE__; goto ret;
  }
  if ( argc >= 3 ) {
    rotation = atoi( argv[2] );
  }
#else
  if ( argc != 6 ) {
    printf("usage : %s srcLeft srcTop srcWidth srcHeight <index(0..2)>\n", argv[0] );
		errLine = __LINE__; goto ret;
  }

  index = atoi( argv[5] );
  if ( index < 0 || index > 2 ) {
		errLine = __LINE__; goto ret;
  }
  g.srcLeft   = atoi( argv[1] );
  g.srcTop    = atoi( argv[2] );
  g.srcWidth  = atoi( argv[3] );
  g.srcHeight = atoi( argv[4] );
  rotation = 1;
  printf("index=%d srcLeft=%d srcTop=%d srcWidth=%d srcHeigh=%d\n",
         index, g.srcLeft, g.srcTop, g.srcWidth, g.srcHeight );
#endif


  fdFB = open("/dev/graphics/fb0", O_RDWR, 0);
	if ( fdFB <= 0 ) {
		errLine = __LINE__; goto ret;
	}

  sizeFBALL = round_up_to_page_size(sizeFB * NUM_BUFFER);
	vaddr = mmap(0, sizeFBALL, PROT_READ | PROT_WRITE, MAP_SHARED, fdFB, 0);
	if (vaddr == MAP_FAILED) 	{
		errLine = __LINE__; goto ret;
	}

  
  // blit( in the frame buffer, copy from rectSrc to rectDst
  {
		int  ion_client;
		int   ion_hnd;
    int const yOffset = SCREEN_HEIGHT * index;
    int  share_fdFB, share_fdTmp;
    int const   sizeTmpBuff = SCREEN_HEIGHT * SCREEN_STRIDE * PIXEL_SIZE_IN_BYTES;

    //hwc_rect_t rectSrc = { 0, 0, 1024, 100 }; //      left  top  right  bottom;
    //hwc_rect_t rectSrc = { 0, 0, 512, 100 }; //      left  top  right  bottom;
    //hwc_rect_t rectSrc = { 64, 16, 256, 128 }; //      left  top  right  bottom;
    hwc_rect_t rectSrc = { 0, 0, 1024, 600 }; //      left  top  right  bottom;
    //hwc_rect_t rectSrc = { 0, 0, 100, 100 }; //      left  top  right  bottom;
    //hwc_rect_t rectSrc = { 0, 0, 100, 200 }; //      left  top  right  bottom;
    //hwc_rect_t rectDst = { 400, 300, 1024, 600 };
    //hwc_rect_t rectDst = { 400, 300, 1020, 500 };
    //hwc_rect_t rectDst = { 400, 300, 1000, 500 };
    //hwc_rect_t rectDst = { 512, 300, 612, 400 };
    //hwc_rect_t rectDst = { 512, 300, 1024, 428 };
    hwc_rect_t rectDst = { 512, 300, 768, 428 };
    int fdRGA = open("/dev/rga", O_RDWR, 0);
		if ( ioctl(fdFB, /*FBIOGET_DMABUF*/0x5003, &share_fdFB) != 0 ) {
      errLine = __LINE__; goto ret;
    }
    if ( fdRGA <= 0 ) {
      errLine = __LINE__; goto ret;
    }
    rectDst.top    += yOffset;
    rectDst.bottom += yOffset;
    

		ion_client = ion_open();
    if ( ion_client <= 0 ) {
      errLine = __LINE__; goto ret;
    }
    if ( ion_alloc( ion_client, sizeTmpBuff, 0, 1, 0, &ion_hnd ) ) {//1(ION_HEAP_SYSTEM_MASK)
      errLine = __LINE__; goto ret;
    }
		if ( ion_share( ion_client, ion_hnd, &share_fdTmp ) ) {
      errLine = __LINE__; goto ret;
    };
    //cpu_ptr = mmap(NULL, size, map_mask, MAP_SHARED, shared_fd, 0);
    //if (MAP_FAILED == cpu_ptr)
    //  errLine = __LINE__; goto ret;
    //}
    hwc_rect_t rectFull = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT }; //      left  top  right  bottom;
    hwcBlit( fdRGA, rectFull, rectFull, share_fdFB, share_fdTmp, SCREEN_HEIGHT*NUM_BUFFER, 0 );

    hwcBlit( fdRGA, rectSrc, rectDst, share_fdTmp, share_fdFB, SCREEN_HEIGHT, rotation );
    if (ioctl(fdRGA, RGA_FLUSH, NULL) != 0) {
      errLine = __LINE__; goto ret;
    }

    ion_close( ion_client );

  }


  
 ret:
  if ( errLine > 0 ) {
    printf("error: line=%d, %s\n", errLine, strerror(errno) );
  } else
    printf("OK\n");
  if ( fdFB > 0 )
    close(fdFB);

  //sleep( 2000 );
  return 0;
}


