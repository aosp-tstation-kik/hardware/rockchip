#!/bin/bash 

SRC_FILE=vsync_test.cpp
OBJ_FILE=vsync_test.o
EXE_FILE=vsync_test

AOSP_ROOT=~/Prog-work/aosp/work
BIN_DIR=$AOSP_ROOT/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.7/bin/


$BIN_DIR/arm-linux-androideabi-g++ -c \
    -I $AOSP_ROOT/libnativehelper/include/nativehelper \
    -isystem $AOSP_ROOT/system/core/include \
    -isystem $AOSP_ROOT/hardware/libhardware/include \
    -isystem $AOSP_ROOT/hardware/libhardware_legacy/include \
    -isystem $AOSP_ROOT/hardware/ril/include \
    -isystem $AOSP_ROOT/libnativehelper/include \
    -isystem $AOSP_ROOT/frameworks/native/include \
    -isystem $AOSP_ROOT/frameworks/native/opengl/include \
    -isystem $AOSP_ROOT/frameworks/av/include \
    -isystem $AOSP_ROOT/frameworks/base/include \
    -isystem $AOSP_ROOT/external/skia/include \
    -isystem $AOSP_ROOT/out/target/product/t-station/obj/include \
    -isystem $AOSP_ROOT/bionic/libc/arch-arm/include \
    -isystem $AOSP_ROOT/bionic/libc/include \
    -isystem $AOSP_ROOT/bionic/libstdc++/include \
    -isystem $AOSP_ROOT/bionic/libc/kernel/common \
    -isystem $AOSP_ROOT/bionic/libc/kernel/arch-arm \
    -isystem $AOSP_ROOT/bionic/libm/include \
    -isystem $AOSP_ROOT/bionic/libm/include/arm \
    -isystem $AOSP_ROOT/bionic/libthread_db/include \
    -fno-exceptions -Wno-multichar -msoft-float -fpic -fPIE -ffunction-sections -fdata-sections -funwind-tables -fstack-protector -Wa,--noexecstack -Werror=format-security -D_FORTIFY_SOURCE=2 -fno-short-enums -march=armv7-a -mfloat-abi=softfp -mfpu=neon \
    -include $AOSP_ROOT/build/core/combo/include/arch/linux-arm/AndroidConfig.h \
    -I $AOSP_ROOT/build/core/combo/include/arch/linux-arm/ \
    -Wno-unused-but-set-variable -fno-builtin-sin -fno-strict-volatile-bitfields -Wno-psabi -mthumb-interwork -DANDROID -fmessage-length=0 -W -Wall -Wno-unused -Winit-self -Wpointer-arith -Werror=return-type -Werror=non-virtual-dtor -Werror=address -Werror=sequence-point -DNDEBUG -g -Wstrict-aliasing=2 -fgcse-after-reload -frerun-cse-after-loop -frename-registers -DNDEBUG -UDEBUG -fvisibility-inlines-hidden -DANDROID -fmessage-length=0 -W -Wall -Wno-unused -Winit-self -Wpointer-arith -Wsign-promo -Werror=return-type -Werror=non-virtual-dtor -Werror=address -Werror=sequence-point -DNDEBUG -UDEBUG -mthumb -Os -fomit-frame-pointer -fno-strict-aliasing  -fno-rtti \
    -DLOG_TAG=\"DummySensor\"     -MD \
         -o $OBJ_FILE \
         $SRC_FILE


$BIN_DIR/arm-linux-androideabi-g++ -v \
    -L$AOSP_ROOT/out/target/product/t-station/obj/lib \
    -nostdlib -Bdynamic -fPIE -pie -Wl,-dynamic-linker,/system/bin/linker -Wl,--gc-sections -Wl,-z,nocopyreloc \
    -Wl,-rpath-link=out/target/product/t-station/obj/lib \
    $AOSP_ROOT/out/target/product/t-station/obj/lib/crtbegin_dynamic.o \
    $OBJ_FILE \
    -Wl,--whole-archive \
    -Wl,--no-whole-archive \
    $AOSP_ROOT/out/target/product/t-station/obj/STATIC_LIBRARIES/libcompiler_rt-extras_intermediates/libcompiler_rt-extras.a \
    -ldl -lcutils -llog -lbinder -lutils -lc -lstdc++ -lm  \
    -o $EXE_FILE \
    -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now -Wl,--warn-shared-textrel -Wl,--fatal-warnings \
    -Wl,--icf=safe -Wl,--fix-cortex-a8   -Wl,--no-undefined \
    $BIN_DIR/../lib/gcc/arm-linux-androideabi/4.7/armv7-a/libgcc.a \
    $AOSP_ROOT/out/target/product/t-station/obj/lib/crtend_android.o



