/*
 * rockchip hwcomposer( 2D graphic acceleration unit) .
 *
 * Copyright (C) 2015 Rockchip Electronics Co., Ltd.
 *
 * modified for t-station(rk3126) 2019/02
 */

// std-c
#include <sys/prctl.h>
#include <time.h>
#include <errno.h>
#include <poll.h>
#include <sys/epoll.h>
#include <sync/sync.h>  // for sync_wait
#include <climits>  // for INT_MAX/MIN
#include <cstdio>   // for vsnprintf
#include <cstdarg>  // for vsnprintf
// c++
#include <algorithm>  // for min and max
// android
#include <cutils/properties.h> 
#include <utils/misc.h>   // for NELEM
#define ENABLE_DEBUG_LOG
#include <log/custom_log.h>
#include <ui/PixelFormat.h>
#include <hardware/hardware.h>  // for hw_module_t
// hwcomposer
#include "rk_debug.h"
#include "rk_hwcomposer.h"
#include "version.h"
#include  <gralloc_priv.h>  // for private_handle_t::PRIV_FLAGS_FRAMEBUFFER


//======================================
// forward declare etc
//======================================
static inline struct private_handle_t const&  privateHandle( buffer_handle_t handle );
class LayerGeometry;

//======================================
// context anchor
//======================================
static hwcContext * gcontextAnchor[HWC_NUM_DISPLAY_TYPES] = {0};


//======================================
// module related
//======================================

static int  hwc_device_open(const struct hw_module_t * module,const char * name,struct hw_device_t ** device);
static struct hw_module_methods_t hwc_module_methods = {
  open:
  hwc_device_open
};

hwc_module_t HAL_MODULE_INFO_SYM =
{
  common:
  {
    tag:
    HARDWARE_MODULE_TAG,
    version_major:
    1,
    version_minor:
    2,
    id:
    HWC_HARDWARE_MODULE_ID,
    name:          "Hardware Composer Module"
    ,
    author:        "Rockchip Corporation"
    ,
    methods:
    &hwc_module_methods,
    dso:
    NULL,
    reserved:
    {
        0,
    }
  }
};


//======================================
// utility
//======================================
// this macro needs variable 'rc' and label 'ret:' .
#define DUMP(buff,len,fmt,...) if ((rc=::dump(buff,len,fmt,__VA_ARGS__))<=0) goto ret;
#define DUMP0(buff,len,fmt)     if ((rc=::dump(buff,len,fmt))<=0) goto ret;
#define DUMPX(dump)     if ((rc=dump)<=0) goto ret;

int dump( char*&buff, int& len, char const* format, ...)
{
    int rc;
    va_list arg;

    if ( len <= 0 )
        return 0;
      
    va_start(arg, format);
    rc = vsnprintf(buff, len, format, arg);
    va_end(arg);

    if ( rc < 0 ) {
        len = 0;
        return rc;
    }
    buff += rc;
    len  -= rc;
    return rc;
}

/*
  dump integer(uint32_t) as binary format.
 */
int dumpBinary( char*& buff, int& len, int val, int bitSize=sizeof(int)*8, char zero=' ', char one='x', char const* pSep="|", int chunkSize=4 )
{
  int const bitSizeOfVal = bitSize;
  int const cntSep = ( bitSizeOfVal - 1 ) / chunkSize;
  int const widthSep = strlen( pSep );
  int const sizeBuff = bitSizeOfVal +1 + cntSep * widthSep;  // +1(null)

  if ( len < sizeBuff ) 
    return -1;
  
  buff[sizeBuff - 1] = 0;
  int   pos = sizeBuff - 2;
  for ( int bitPos = 0 ; bitPos < bitSizeOfVal ; bitPos ++, pos -- ) {
    if ( bitPos && bitPos % chunkSize == 0 ) {
      memcpy( &buff[pos-widthSep+1], pSep, widthSep );
      pos -= widthSep;
    }
    buff[pos] = val & (1<<bitPos) ? one : zero;
  }

  len  -= sizeBuff - 1;
  buff += sizeBuff - 1;
  return sizeBuff - 1;
}


//======================================
// policy table
//======================================
// vop_gpu_reuse_policy covers most of the vop_rga_policy's functionality. 
#define USE_VOP_RGA_POLICY  0

typedef int (*TRY_POLICY_FNC)( hwcContext&, hwc_display_contents_1_t&, LayerGeometry const& );
typedef int (*SET_POLICY_FNC)( hwcContext&, hwc_display_contents_1_t&, LayerGeometry const& );

static int try_vop_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
static int set_vop_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
#if USE_VOP_RGA_POLICY
static int try_vop_rga_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
static int set_vop_rga_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
#endif
//static int try_vop_gpu_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
//static int set_vop_gpu_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
static int try_vop_gpu_reuse_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
static int set_vop_gpu_reuse_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
static int try_gpu_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );
static int set_gpu_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo );

struct policy_entry {
    char const* strCompose;
    TRY_POLICY_FNC  try_policy_fnc;
    SET_POLICY_FNC  set_policy_fnc;
};

static policy_entry policy_table_primary[] = {
    { "VOP",      try_vop_policy,       set_vop_policy },
#if USE_VOP_RGA_POLICY
    { "VOP_RGA",  try_vop_rga_policy,   set_vop_rga_policy },
#endif
//    { "VOP_GPU",  try_vop_gpu_policy,   set_vop_gpu_policy },
    { "VOP_GPU_REUSE",  try_vop_gpu_reuse_policy,   set_vop_gpu_reuse_policy },
    { "GPU",      try_gpu_policy,       set_gpu_policy }, // gpu policy must be last.
};

inline bool   isValidPolicy( policy_entry const& policy )
{
    int const     offset = (intptr_t)&policy - (intptr_t)policy_table_primary;
    int const     modulo = offset % sizeof(policy_entry);

    return 0 <= offset && offset < (int)sizeof(policy_table_primary) &&  modulo == 0;
}
    
//======================================
// debug 
//======================================
#define DEBUG_CODE(...)  { __VA_ARGS__ }
#define DEBUG_CODE0()  

// for debug
static struct DebugInfo {
    // constant
    enum {
        MAX_LAYER = 32,
        MAX_RECTS = 32,
    };

    // member variable
    int   cntPrepare;
    int   index;    // index of frame buffer
    int   numLayer;
    struct layer_ {
        hwc_layer_1_t const*  pLayer;
        int32_t         compositionType;

        int             numRects;
        hwc_rect_t      rects[MAX_RECTS];
    } layer[MAX_LAYER];

    // constructor
    DebugInfo() : cntPrepare(0), index(-1), numLayer(-1) {};

    // modifier
    void    save( hwc_display_contents_1_t const& disp );

    // accessor
    int     check( hwc_display_contents_1_t const& disp ) const;

    // utility
    int     dump( char*& buff, int& len ) const;

} debugInfo_g;

/*
  return -1 if an error occours.
 */
int   DebugInfo::check( hwc_display_contents_1_t const& disp ) const
{
    int const   numLayerLimited = std::min( (int)disp.numHwLayers, (int)DebugInfo::MAX_LAYER );

    if ( numLayer != numLayerLimited ) {
        ALOGE("@@@@ logical error %s(%d) : %d %d", __FUNCTION__, __LINE__, numLayer, numLayerLimited );
        goto err;
    }

    for ( int i = 0 ; i < numLayerLimited ; i ++ ) {
        if ( layer[i].pLayer != &disp.hwLayers[i] ) {
            ALOGE("@@@@ logical error %s(%d) : %d", __FUNCTION__, __LINE__, i );
            goto err;
        }
    }

    return 0;
  err:
    for ( int i = 0 ; i < numLayer ; i ++ ) {
        DebugInfo::layer_ const& lG = layer[i];
        ALOGE("@@@@ %p %d %d", lG.pLayer, lG.compositionType, lG.numRects );
    }
    for ( int i = 0 ; i < (int)disp.numHwLayers ; i ++ ) {
        hwc_layer_1_t const&  lH = disp.hwLayers[i];
        ALOGE("@@@@ %p %d %d", &lH, lH.compositionType, lH.visibleRegionScreen.numRects );
    }
    return -1;
}

void  DebugInfo::save( hwc_display_contents_1_t const& disp )
{
    cntPrepare ++;

    if ( &disp == 0 ) {
        ALOGE("@@@@ %s(%d)", __FUNCTION__, __LINE__ );
        return;
    }

    int const   numLayerLimited = std::min( (int)disp.numHwLayers, (int)DebugInfo::MAX_LAYER );

    if ( numLayerLimited == 0 ) {
        numLayer = 0;
        ALOGE("@@@@ %s(%d)", __FUNCTION__, __LINE__ );
        return;
    }

    // layer[0..g.numLayer-1] <--
    numLayer = numLayerLimited;
    for ( int i = 0 ; i < numLayerLimited ; i ++ ) {
        hwc_layer_1_t const&    layerSrc = disp.hwLayers[i];
        DebugInfo::layer_&      layerDst = layer[i];

        layerDst.pLayer = &layerSrc;
        layerDst.compositionType = layerSrc.compositionType;

        if ( layerSrc.compositionType == HWC_BACKGROUND ) {
            layerDst.numRects = 0;
            continue;
        }

        // when  hwc_dump() is called, hwc_layer_1_t.visibleRegionScreen is already cleared,
        // so save it for later use.
        int const numRectsLimited = std::min( (int)layerSrc.visibleRegionScreen.numRects, (int)DebugInfo::MAX_RECTS );
        layerDst.numRects = layerSrc.visibleRegionScreen.rects ? numRectsLimited : 0;
        for ( int j = 0 ; j < layerDst.numRects ; j ++ ) {
            layerDst.rects[j] = layerSrc.visibleRegionScreen.rects[j];
        }
    }


    // index <--
    hwc_layer_1_t const&    layerFB = disp.hwLayers[numLayer-1];
    private_handle_t const& handle = privateHandle( layerFB.handle );
    if ( &handle == 0 ) {
        // at boot time, the frame buffer target has null handle.
        index = -1;
        ALOGE("@@@@ frame buffer target handle = %p", &handle );
    } else {
        index = handle.offset / handle.size;
    }

    //ret:;
}

/*
  return:
    -1 if error occured,
    count of bytes which are pushed into buff.
 */
int  DebugInfo::dump( char*& buff, int& len ) const
{
    //int const lenOrg = len;
    int   rc;
    const char * compositionName[] = {
        "FRAMEBUFFER",
        "OVERLAY",
        "BACKGROUND",
        "FB_TARGET",
        "SIDEBAND",
        "CURSOR_OVERLAY",
        "TOWIN0",
        "TOWIN1",
        "LCDC",
        "NODRAW",
        "MIX",
        "MIX_V2",
        "BLITTER",
        "DIM",
        "CLEAR_HOLE",
        "out of range?"
    };

    // header
    DUMP( buff, len, "debugInfo_g index=%d prepare=%d\n", index, cntPrepare );
    DUMP0( buff, len, "  composition al handle      (fg widt heig fm strd) rects\n" );

    // each layer
    for ( int i = 0 ; i < numLayer ; i ++ ) {
        DebugInfo::layer_  const&  layerG = layer[i];
        hwc_layer_1_t const&      layerH = *layerG.pLayer;
        int const                 compositionType = std::min( layerG.compositionType, NELEM(compositionName) - 1 );
        char const*               pName = compositionName[compositionType];
        private_handle_t const&   handle = privateHandle( layerH.handle );

        DUMP( buff, len, "  %-11.11s %2.2x %10.10p", pName, (int)layerH.planeAlpha, &handle );
        if ( layerH.compositionType == HWC_BACKGROUND || &handle == 0 ) {
            DUMP0( buff, len, "                     " );
        } else {
            DUMP( buff, len, "(%2.2x %4d %4d %2.2x %4d)",
                  handle.flags,
                  handle.width,
                  handle.height,
                  handle.format,
                  handle.stride );
        }

        for ( int j = 0 ; j < layerG.numRects ; j ++ ) {
            hwc_rect_t const& rect = layerG.rects[j];
            DUMP( buff, len, " [%4d %4d %4d %4d]", rect.left, rect.top, rect.right, rect.bottom );
        }
        // cr/lf
        DUMP0( buff, len, "\n" );
    }

  ret:
    return rc;
}


// these function is used in other sources too, so not to deaclare static.
void is_debug_log(void)
{
    hwcContext * context = gcontextAnchor[HWC_DISPLAY_PRIMARY];

    context->Is_debug = hwc_get_int_property("sys.hwc.log","0");
}

int is_out_log( void )
{
    hwcContext * context = gcontextAnchor[HWC_DISPLAY_PRIMARY];

    return context->Is_debug;
}

void stop_debug_log(void)
{
  
    hwcContext * context = gcontextAnchor[HWC_DISPLAY_PRIMARY];
    property_set("sys.hwc.log", "0");
    context->Is_debug = false;
}


//======================================
// rect,region handling
//======================================
inline bool isEmpty( hwc_rect_t const& rect )
{
    return rect.right <= rect.left || rect.bottom <= rect.top;
}

inline int area( hwc_rect_t const& rect )
{
    return ( rect.right - rect.left ) * ( rect.bottom - rect.top );
}

static int area( hwc_region_t const& region )
{
    int   ar = 0;
    for ( int i = 0 ; i < (int)region.numRects ; i ++ )
        ar += area( region.rects[i] );
    return ar;
}

inline bool operator !=( hwc_rect_t const& r1, hwc_rect_t const& r2 )
{
    bool  rc =  r1.left != r2.left || r1.right != r2.right || r1.top != r2.top || r1.bottom != r2.bottom;
    return rc;
}
inline bool operator ==( hwc_rect_t const& r1, hwc_rect_t const& r2 )
{
  
    return !( r1 != r2 );
}

inline void intersect( hwc_rect_t const& rect1, hwc_rect_t const& rect2, int&left, int&right, int&top, int&bottom )
{
    left   = std::max( rect1.left, rect2.left ); 
    right  = std::min( rect1.right, rect2.right ); 
    top    = std::max( rect1.top, rect2.top ); 
    bottom = std::min( rect1.bottom, rect2.bottom ); 
}

/*
  return true  : if 2 rects intersect, and rectIntersect == intersection of the 2 rects.
  return false : otherwise
*/
static bool  intersect( hwc_rect_t const& rect1, hwc_rect_t const& rect2 )
{
    int l, r, t, b;
    intersect( rect1, rect2, l, r, t, b );

    return l < r && t < b;
}

/*
  return 1 : if 2 rects intersect, and rectIntersect == intersection of the 2 rects.
  return 0 : otherwise, and rectIntersect={0,0,0,0}
*/
#if USE_VOP_RGA_POLICY
static int intersect( hwc_rect_t const& rect1, hwc_rect_t const& rect2, hwc_rect_t& rectIntersect )
{
    int rc = 0;
    int l, r, t, b;
    intersect( rect1, rect2, l, r, t, b );

    if ( r <= l || b <= t ) {
        l = r = t = b = 0;
        rc = 0;
        goto ret;
    }

    rc = 1;

  ret:
    rectIntersect.left   = l;
    rectIntersect.right  = r;
    rectIntersect.top    = t;
    rectIntersect.bottom = b;
    return rc;
}
#endif
/*
  return true  : if 2 rects intersect, and rectIntersect == intersection of the 2 rects.
  return false : otherwise
*/
static bool  intersect( hwc_region_t const& reg1, hwc_region_t const& reg2 )
{

    for ( int i = 0 ; i < (int)reg1.numRects ; i ++ ) {
        hwc_rect_t const&   rect1 = reg1.rects[i];
        for ( int j = 0 ; j < (int)reg2.numRects ; j ++ ) {
            hwc_rect_t const&   rect2 = reg2.rects[j];
            if ( intersect( rect1, rect2 ) )
                return true;
        }
    }
    return false;
}

/*
  rectBound == bound of r1 and r2
*/
inline  void  bound( hwc_rect_t const& r1, hwc_rect_t const& r2, hwc_rect_t& rectBound )
{
    rectBound.left   = std::min( r1.left,   r2.left );
    rectBound.right  = std::max( r1.right,  r2.right );
    rectBound.top    = std::min( r1.top,    r2.top );
    rectBound.bottom = std::max( r1.bottom, r2.bottom );
}

/*
  return 1 : if the bound rect is not empty, 
  retorn 0 : otherwise, and rectBound=={0,0,0,0}
*/
#if 0 // not used
static int bound( hwc_region_t const& region, hwc_rect_t& rectBound )
{
    int rc = 1;
    hwc_rect_t const* pRects = region.rects;

    if ( region.numRects == 0 || pRects == 0 ) {
        rectBound.left = rectBound.right = rectBound.top = rectBound.bottom = 0;
        rc = 0;
        goto ret;
    }

    rectBound = *pRects;
    for ( int cnt = region.numRects ; -- cnt > 0 ; ) {
        pRects ++;
        bound( rectBound, *pRects, rectBound );
    }

  ret:
    return rc;
}
#endif

//======================================
// private_handle_t related
//======================================
void  getVirtual( struct private_handle_t const& handle, uint32_t&widthVir, uint32_t&heightVir, uint32_t&yOffset)
{
    if ( handle.flags & private_handle_t::PRIV_FLAGS_FRAMEBUFFER ) {
        yOffset   = handle.offset / handle.size * handle.height;
        heightVir = yOffset + handle.height;  // this should be handle.height * NUM_FB_BUFFERS( defined as 3 in ../libgralloc/gralloc_priv.h )
        widthVir  = handle.stride;//rkmALIGN( handle.width, 32 );
    } else {
        yOffset   = 0;
        heightVir = handle.height;
        widthVir  = handle.stride;
    }
}

static inline struct private_handle_t const&  privateHandle( buffer_handle_t handle )
{
    return *reinterpret_cast<struct private_handle_t const*>(handle);
}


//======================================
// RGA routine
//======================================
#if USE_VOP_RGA_POLICY
static bool rgaHasAlpha(RgaSURF_FORMAT format)
{
  switch ( format ) {
  case RK_FORMAT_RGBA_8888:
    //case RK_FORMAT_RGBX_8888:
    //case RK_FORMAT_RGB_888:
  case RK_FORMAT_BGRA_8888:
    //case RK_FORMAT_RGB_565:
  case RK_FORMAT_RGBA_5551:
  case RK_FORMAT_RGBA_4444:
    //case RK_FORMAT_BGR_888:
    return true;
  default:;
  }
  return false;
}

static void rgaGetBufferInfo(
    struct private_handle_t const& handle,
    void**        pLogical,
    unsigned int* pWidth,
    unsigned int* pHeight,
    unsigned int* pStride,
    unsigned int* pHeightVir,
    unsigned int* pHeightOfs
    
    )
{
    *pLogical       = (void*)handle.base;
    *pHeight        = handle.height;
    *pStride        = handle.stride;
    getVirtual( handle, *pWidth, *pHeightVir, *pHeightOfs );
}

static hwcSTATUS   rgaFormat(
     struct private_handle_t const& handle,
     RgaSURF_FORMAT * Format
    )
{
    switch (handle.format) {
    case HAL_PIXEL_FORMAT_RGB_565:
        *Format = RK_FORMAT_RGB_565;
        break;
    case HAL_PIXEL_FORMAT_RGBA_8888:
        *Format = RK_FORMAT_RGBA_8888;
        break;
    case HAL_PIXEL_FORMAT_RGBX_8888:
        *Format = RK_FORMAT_RGBX_8888;
        break;
    case HAL_PIXEL_FORMAT_BGRA_8888:
        *Format = RK_FORMAT_BGRA_8888;
        break;
    case HAL_PIXEL_FORMAT_YCrCb_NV12:
        /* YUV 420 semi planner: NV12 */
        *Format = RK_FORMAT_YCbCr_420_SP;
        break;
    case HAL_PIXEL_FORMAT_YCrCb_420_SP:  // NV21
        *Format = RK_FORMAT_YCrCb_420_SP;
        break;
		case HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO:
        *Format = RK_FORMAT_YCbCr_420_SP;
        break; 
    default:
        return hwcSTATUS_INVALID_ARGUMENT;
    }

    return hwcSTATUS_OK;
}

static int rgaBlit(
    IN int                  fdRGA,
    IN hwc_layer_1_t const& layerSrc,
    IN struct private_handle_t const& handleDst,
    IN int index
    )
{
    hwcSTATUS status = hwcSTATUS_OK;

    void *              srcLogical;
    unsigned int        srcStride;
    unsigned int        srcWidth;
    unsigned int        srcHeight;
    unsigned int        srcHeightVir;
    unsigned int        srcHeightOfs;
    RgaSURF_FORMAT      srcFormat;

    void *              dstLogical;
    unsigned int        dstStride;
    unsigned int        dstWidth;
    unsigned int        dstHeight;
    unsigned int        dstHeightVir;
    unsigned int        dstHeightOfs;
    RgaSURF_FORMAT      dstFormat;

    struct private_handle_t const& handleSrc = privateHandle(layerSrc.handle);

    hwc_rect_t const& rectSrc = layerSrc.sourceCrop;
    hwc_rect_t const& rectDst = layerSrc.displayFrame;
    hwc_region_t const& region = layerSrc.visibleRegionScreen;


    float hfactor;
    float vfactor;
    int  stretch;
    int  yuvFormatSrc, yuvFormatDst;

    struct rga_req    Rga_Request;
    unsigned char     RotateMode = 0;
    int               Rotation = 0;
    unsigned int      Xoffset;
    unsigned int      Yoffset;
    unsigned int      WidthAct;
    unsigned int      HeightAct;
    unsigned char     scale_mode = 0; //@@@@2;
    int retv  = 0;
    unsigned char   dither_en = android::bytesPerPixel(handleSrc.format) == android::bytesPerPixel(handleDst.format) ? 0 : 1;

    memset(&Rga_Request, 0x0, sizeof(Rga_Request));
    int srcMmuType = handleSrc.type ? 1 : 0;;
    int dstMmuType = handleDst.type ? 1 : 0;
    if (srcMmuType || dstMmuType) {
        RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
        Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (dstMmuType << 10) | (srcMmuType << 8);
    }

    rgaGetBufferInfo(
        handleSrc,
        &srcLogical,
        &srcWidth,
        &srcHeight,
        &srcStride,
        &srcHeightVir,
        &srcHeightOfs
        );

    rgaGetBufferInfo(
        handleDst,
        &dstLogical,
        &dstWidth,
        &dstHeight,
        &dstStride,
        &dstHeightVir,
        &dstHeightOfs
        );

    //@@@@//fix bug of color change when running screenrecord
    //@@@@if (handleDst.format != Context->fbhandle.format)
    //@@@@  {
    //@@@@    ALOGV("Force dst format to fb format: %x => %x", handleDst.format, Context->fbhandle.format);
    //@@@@    //@@@@handleDst.format = Context->fbhandle.format;
    //@@@@    const_cast<struct private_handle_t*>(handleDst.format = Context->fbhandle.format;
    //@@@@  }
    hwcONERROR( rgaFormat(handleSrc, &srcFormat ));
    hwcONERROR( rgaFormat(handleDst, &dstFormat ));
    /* Check yuv format. */
    yuvFormatSrc = (srcFormat >= RK_FORMAT_YCbCr_422_SP && srcFormat <= RK_FORMAT_YCbCr_420_P);
    yuvFormatDst = (dstFormat >= RK_FORMAT_YCbCr_422_SP && dstFormat <= RK_FORMAT_YCbCr_420_P);
    //@@@@if(yuvFormatDst) {
    //@@@@    rectDst.left -= rectDst.left%2;
    //@@@@    rectDst.top -= rectDst.top%2;            
    //@@@@    rectDst.right -= rectDst.right%2;
    //@@@@    rectDst.bottom -= rectDst.bottom%2;
    //@@@@}
    //@@@@ GRALLOC_USAGE_PROTECTED is not supported.
    //@@@@if(handle->usage & GRALLOC_USAGE_PROTECTED) {
    //@@@@    RGA_set_src_vir_info(&Rga_Request, (unsigned long)handle->phy_addr, (unsigned long)handle->phy_addr + (handle->width * handle->height ),  
    //@@@@                         (unsigned long)handle->phy_addr +(handle->width * handle->height ), srcStride, srcHeight, srcFormat, 0);
    //@@@@    RGA_set_dst_vir_info(&Rga_Request, dstFd, reinterpret_cast<int>(handleDst.base),  0, handleDst.stride, dstHeight, &clip, dstFormat, 0);
    //@@@@    rga_set_fds_offsets(&Rga_Request, 0, 0, 0, 0);
    //@@@@} else
    // assume( handle->usage & GRALLOC_USAGE_PROTECTED == 0 )
    {
        RECT clip = { 0, (uint16_t)(dstWidth - 1), (uint16_t)dstHeightOfs, (uint16_t)(dstHeightOfs + dstHeight - 1) }; //xmin xmax ymin ymax
        RGA_set_src_vir_info(&Rga_Request, handleSrc.share_fd, reinterpret_cast<int>(srcLogical),  0, srcStride, srcHeightVir, srcFormat, 0);
        RGA_set_dst_vir_info(&Rga_Request, handleDst.share_fd, reinterpret_cast<int>(dstLogical),  0, dstStride, dstHeightVir, &clip, dstFormat, 0);
    }

    // alpha blending
    if ( index > 0 ) {   // bottom layer donnt need alpha
        bool      perpixelAlpha = rgaHasAlpha(srcFormat);
        uint8_t   planeAlpha = layerSrc.planeAlpha;
        switch ( layerSrc.blending ) {
        case HWC_BLENDING_PREMULT:
          if (perpixelAlpha && planeAlpha < 255) {  // Perpixel and planeAlpha
                RGA_set_alpha_en_info(&Rga_Request, 1, 2, planeAlpha , 1, 9, 0);
            } else if (perpixelAlpha) { // Perpixel only
                RGA_set_alpha_en_info(&Rga_Request, 1, 1, 0, 1, 3, 0);
            } else { /* if (planeAlpha < 255) */ // Plane alpha only. 
                RGA_set_alpha_en_info(&Rga_Request, 1, 0, planeAlpha , 0, 0, 0);
            }

#if 0
            DEBUG_CODE(
                char value[PROPERTY_VALUE_MAX];
                static int i0=1, i1=2, i2=255, i3=1, i4=9, i5=0, i6=0;
                int i0T, i1T, i2T, i3T, i4T, i5T, i6T;
                property_get("sys.test0", value, "");
                int rc = sscanf(value, "%d %d %d %d %d %d %d", &i0T,&i1T,&i2T,&i3T,&i4T,&i5T,&i6T );
                if ( rc == 7 ) {
                    i0=i0T; i1=i1T;i2=i2T;i3=i3T;i4=i4T;i5=i5T;i6=i6T;
                    ALOGD("@@@@[0] %d %d %d %d %d %d %d", i0,i1,i2,i3,i4,i5,i6 );
                    property_set("sys.test0", "");
                }
                if ( index == 2 && i6 ) {
                    RGA_set_alpha_en_info(&Rga_Request, i0, i1, i2, i3, i4, i5);
                }
                );
#endif
            break;

        case HWC_BLENDING_COVERAGE:
            /* SRC_ALPHA / ONE_MINUS_SRC_ALPHA. */
            /* Cs' = Cs * As
             * As' = As
             * C = Cs' + Cd * (1 - As)
             * A = As' + Ad * (1 - As) */
            if (perpixelAlpha && planeAlpha < 255) {  // Perpixel and planeAlpha
                RGA_set_alpha_en_info(&Rga_Request, 1, 2, planeAlpha , 0, 0, 0);
            } else if (perpixelAlpha) { /* Perpixel alpha only. */
                RGA_set_alpha_en_info(&Rga_Request, 1, 1, 0, 0, 0, 0);
            } else {/* if (planeAlpha < 255) */ // Plane alpha only. 
                RGA_set_alpha_en_info(&Rga_Request, 1, 0, planeAlpha , 0, 0, 0);
            }
            break;

        case HWC_BLENDING_NONE:
        default:
            // Tips: BLENDING_NONE is non-zero value, handle zero value as BLENDING_NONE. 
            // make alpha channel be 255.
            RGA_set_alpha_en_info( &Rga_Request, 1, 0, 255, 1, 3, 0 );
            break;
        }
    }    
    
    /* Check stretching. */
    if ((layerSrc.transform == HWC_TRANSFORM_ROT_90) || (layerSrc.transform == HWC_TRANSFORM_ROT_270)) {
        hfactor = (float)(rectSrc.right - rectSrc.left) / (rectDst.bottom - rectDst.top);
        vfactor= (float)(rectSrc.bottom - rectSrc.top) / (rectDst.right - rectDst.left);
    } else {
        hfactor = (float)(rectSrc.right - rectSrc.left) / (rectDst.right - rectDst.left);
        vfactor = (float)(rectSrc.bottom - rectSrc.top) / (rectDst.bottom - rectDst.top);
    }
    stretch = (hfactor != 1.0f) || (vfactor != 1.0f);

    // Go through all visible regions (clip rectangles?). 
    for ( int n = 0 ; n < (int)region.numRects ; ) {
        /* 16 rectangles a batch. */
        int m;
        hwc_rect_t  rectSrcs[16];
        hwc_rect_t  rectDsts[16];
        hwc_rect_t const* rects = region.rects;

        for ( m = 0 ; n < (int)region.numRects && m < 16; n++ ) {
            // Hardware will mirror in dest rect and blit area in clipped rect.
            // But we need mirror area in clippred rect.
            // NOTE: Now we always set rectDst to clip area. 

            // Intersect clip with dest. 
          if ( intersect( rectDst, rects[n], rectDsts[m] ) == 0 )
            continue; // Skip if empty 
            //@@@@rectDsts[m].right  = std::min(rectDsts[m].right, (int)dstWidth);//dstWidth to signed int  can't be negative
            //@@@@if (rectDsts[m].top < 0) {// @ buyudaren grame rectDsts[m].top < 0,bottom is height ,so do this
            //@@@@    rectDsts[m].top = rectDsts[m].top + dstHeight;
            //@@@@    rectDsts[m].bottom = rectDsts[m].bottom + rectDsts[m].top;
            //@@@@}

            if(yuvFormatDst) {
              rectDsts[m].left   &= ~1; //-= rectDsts[m].left%2;
              rectDsts[m].top    &= ~1;  //-= rectDsts[m].top%2;            
              rectDsts[m].right  &= ~1;  //-= rectDsts[m].right%2;
              rectDsts[m].bottom &= ~1; //-= rectDsts[m].bottom%2;
            }
            // Advance to next rectangle. 
            m++;
        }

        // Try next group if no rects. 
        if (m == 0) {
            hwcONERROR(hwcSTATUS_INVALID_ARGUMENT);
        }
        
        for ( int i = 0; i < m ; i++ ) {
            switch (layerSrc.transform) {
            case 0:
                RotateMode = stretch;
                Xoffset = rectDsts[i].left;
                Yoffset = rectDsts[i].top;
                WidthAct = rectDsts[i].right - rectDsts[i].left ;
                HeightAct = rectDsts[i].bottom - rectDsts[i].top ;
                // calculate rectSrcs,rectDst is virtual rect,rectDsts[i] is actual rect,
                //  hfactor and vfactor are scale factor.
                rectSrcs[i].left   = rectSrc.left - (int)((rectDst.left   - rectDsts[i].left)   * hfactor);
                rectSrcs[i].top    = rectSrc.top - (int)((rectDst.top    - rectDsts[i].top)    * vfactor);
                rectSrcs[i].right  = rectSrc.right - (int)((rectDst.right  - rectDsts[i].right)  * hfactor);
                rectSrcs[i].bottom = rectSrc.bottom - (int)((rectDst.bottom - rectDsts[i].bottom) * vfactor);

                break;
            case HWC_TRANSFORM_FLIP_H:
                RotateMode      = 2;
                Xoffset = rectDsts[i].left;
                Yoffset = rectDsts[i].top;
                WidthAct = rectDsts[i].right - rectDsts[i].left ;
                HeightAct = rectDsts[i].bottom - rectDsts[i].top ;
                rectSrcs[i].left   = srcWidth - (rectSrc.right - (int)((rectDst.right  - rectDsts[i].right)  * hfactor));
                rectSrcs[i].top    = rectSrc.top - (int)((rectDst.top    - rectDsts[i].top)    * vfactor);
                rectSrcs[i].right  = srcWidth - (rectSrc.left - (int)((rectDst.left   - rectDsts[i].left)   * hfactor));
                rectSrcs[i].bottom = rectSrc.bottom - (int)((rectDst.bottom - rectDsts[i].bottom) * vfactor);

                break;
            case HWC_TRANSFORM_FLIP_V:
                RotateMode      = 3;
                Xoffset = rectDsts[i].left;
                Yoffset = rectDsts[i].top;
                WidthAct = rectDsts[i].right - rectDsts[i].left ;
                HeightAct = rectDsts[i].bottom - rectDsts[i].top ;
                rectSrcs[i].left   = rectSrc.left - (int)((rectDst.left   - rectDsts[i].left)   * hfactor);
                rectSrcs[i].top    = srcHeight - (rectSrc.bottom - (int)((rectDst.bottom - rectDsts[i].bottom) * vfactor));
                rectSrcs[i].right  = rectSrc.right - (int)((rectDst.right  - rectDsts[i].right)  * hfactor);
                rectSrcs[i].bottom = srcHeight - (rectSrc.top - (int)((rectDst.top    - rectDsts[i].top)    * vfactor));
                break;

            case HWC_TRANSFORM_ROT_90:
                RotateMode      = 1;
                Rotation    = 90;
                Xoffset = rectDsts[i].right - 1;
                Yoffset = rectDsts[i].top ;
                WidthAct = rectDsts[i].bottom - rectDsts[i].top ;
                HeightAct = rectDsts[i].right - rectDsts[i].left ;
                rectSrcs[i].left   = rectSrc.left + (int)((rectDsts[i].top -  rectDst.top) * hfactor);
                rectSrcs[i].top    =  rectSrc.top + (int)((rectDst.right - rectDsts[i].right) * vfactor);
                rectSrcs[i].right  = rectSrcs[i].left + (int)((rectDsts[i].bottom - rectDsts[i].top) * hfactor);
                rectSrcs[i].bottom = rectSrcs[i].top + (int)((rectDsts[i].right  - rectDsts[i].left) * vfactor);
                break;

            case HWC_TRANSFORM_ROT_180:
                RotateMode      = 1;
                Rotation    = 180;
                Xoffset = rectDsts[i].right - 1;
                Yoffset = rectDsts[i].bottom - 1;
                WidthAct = rectDsts[i].right - rectDsts[i].left;
                HeightAct = rectDsts[i].bottom - rectDsts[i].top;
                //rectSrcs[i].left   = rectSrc.left +  (rectSrc.right - rectSrc.left)
                //- ((rectDsts[i].right - rectDsts[i].left) * hfactor)
                //+ ((rectDst.left   - rectDsts[i].left)   * hfactor);
                rectSrcs[i].left   = rectSrc.left + (int)((rectDst.right - rectDsts[i].right)   * hfactor);
                rectSrcs[i].top    =  rectSrc.top + (int)((rectDst.bottom - rectDsts[i].bottom)   * vfactor);
                rectSrcs[i].right  = rectSrcs[i].left + (int)((rectDsts[i].right  - rectDsts[i].left) * hfactor);
                rectSrcs[i].bottom = rectSrcs[i].top + (int)((rectDsts[i].bottom - rectDsts[i].top) * vfactor);
                break;

            case HWC_TRANSFORM_ROT_270:
                
                RotateMode      = 1;
                Rotation        = 270;
                Xoffset = rectDsts[i].left;
                Yoffset = rectDsts[i].bottom - 1;
                WidthAct = rectDsts[i].bottom - rectDsts[i].top ;
                HeightAct = rectDsts[i].right - rectDsts[i].left ;
                //rectSrcs[i].left   = rectSrc.top +  (rectSrc.right - rectSrc.left)
                //- ((rectDsts[i].bottom - rectDsts[i].top) * vfactor)
                //+ ((rectDst.top    - rectDsts[i].top)    * vfactor);
                rectSrcs[i].left   = rectSrc.left + (int)((rectDst.bottom - rectDsts[i].bottom )   * hfactor);
                rectSrcs[i].top    =  rectSrc.top + (int)((rectDsts[i].left - rectDst.left )   * vfactor);
                rectSrcs[i].right  = rectSrcs[i].left + (int)((rectDsts[i].bottom - rectDsts[i].top) * hfactor);
                rectSrcs[i].bottom = rectSrcs[i].top + (int)((rectDsts[i].right  - rectDsts[i].left) * vfactor);
                break;
            default:
                hwcONERROR(hwcSTATUS_INVALID_ARGUMENT);
                break;
            }

            rectSrcs[i].left = std::max(rectSrcs[i].left,0);
            rectSrcs[i].top = std::max(rectSrcs[i].top,0); 
            rectSrcs[i].right =  std::min(rectSrcs[i].right,rectSrc.right);
            rectSrcs[i].bottom =  std::min(rectSrcs[i].bottom,rectSrc.bottom);

            if (yuvFormatSrc) {
              rectSrcs[i].left   &= ~1;	//-=  rectSrcs[i].left % 2; 
              rectSrcs[i].top    &= ~1;	//-=  rectSrcs[i].top % 2;               
              rectSrcs[i].right  &= ~1;	//-=  rectSrcs[i].right % 2; 
              rectSrcs[i].bottom &= ~1;	//-=  rectSrcs[i].bottom % 2;
            }
            RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);
            RGA_set_src_act_info(&Rga_Request, rectSrcs[i].right -  rectSrcs[i].left, rectSrcs[i].bottom - rectSrcs[i].top,  rectSrcs[i].left, rectSrcs[i].top + srcHeightOfs );
            RGA_set_dst_act_info(&Rga_Request, WidthAct, HeightAct, Xoffset, Yoffset + dstHeightOfs );//@@@@

            retv = ioctl(fdRGA, RGA_BLIT_ASYNC, &Rga_Request);
            if( retv != 0) {
                LOGE("RGA ASYNC err=%d,name=%s",retv, layerSrc.LayerName);
            }
        }
    }

    return hwcSTATUS_OK;

  OnError:
    LOGE("%s(%d):  Failed", __FUNCTION__, __LINE__);
    /* Error roll back. */
    /* Unlock buffer. */
    return status;
}

static int  rgaClear(
    int   fdRGA,
    IN unsigned int Color,
    IN int          blending,
    IN struct private_handle_t const& handleDst,
    IN hwc_rect_t const& rectDst,
    IN hwc_region_t const& regionDst
)
{
    hwcSTATUS         status = hwcSTATUS_OK;
    void *            dstLogical;
    unsigned int      dstStride;
    unsigned int      dstWidth;
    unsigned int      dstHeight;
    unsigned int      dstHeightVir;
    unsigned int      dstHeightOfs;
    RgaSURF_FORMAT    dstFormat;

    struct rga_req    Rga_Request;
    COLOR_FILL        FillColor;

    memset(&FillColor , 0x0, sizeof(COLOR_FILL));
    memset(&Rga_Request, 0x0, sizeof(Rga_Request));

    int srcMmuType = 0;
    int dstMmuType = handleDst.type ? 1 : 0;
    if (dstMmuType) {
      RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
      Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (dstMmuType << 10) | (srcMmuType << 8);
    }
    RGA_set_color_fill_mode(&Rga_Request, &FillColor, 0, 0, Color, 0, 0, 0, 0, 0);

    int               planeAlpha = (unsigned int)Color >> 24;
    if ( planeAlpha != 0xff ) {
      // Setup blending. 
      switch ( blending ) {
      case HWC_BLENDING_PREMULT:
      case HWC_BLENDING_COVERAGE:
        // Plane alpha only. 
        RGA_set_alpha_en_info(&Rga_Request, 1, 0, planeAlpha , 0, 0, 0);
        break;
      default:;
#if 0
          DEBUG_CODE(
                char value[PROPERTY_VALUE_MAX];
                static int i0=1, i1=0, i2=0, i3=0, i4=0, i5=0, i6=0 ;
                int i0T, i1T, i2T, i3T, i4T, i5T, i6T;
                property_get("sys.testC", value, "");
                int rc = sscanf(value, "%d %d %d %d %d %d %d", &i0T,&i1T,&i2T,&i3T,&i4T,&i5T,&i6T );
                if ( rc == 7 ) {
                    i0=i0T; i1=i1T;i2=i2T;i3=i3T;i4=i4T;i5=i5T;i6=i6T;
                    ALOGD("@@@@[C] %d %d %d %d %d %d %d", i0,i1,i2,i3,i4,i5,i6 );
                    property_set("sys.testC", "");
                }
                if (i6 )
                    RGA_set_alpha_en_info(&Rga_Request, i0, i1, i2, i3, i4, i5);
                );
#endif          

      }
    }    

    rgaGetBufferInfo( handleDst,
                      &dstLogical,
                      &dstWidth,
                      &dstHeight,
                      &dstStride,
                      &dstHeightVir,
                      &dstHeightOfs
                      );
    hwcONERROR( rgaFormat(handleDst, &dstFormat ));

    {
      RECT clip = { 0, (uint16_t)(dstWidth - 1), (uint16_t)dstHeightOfs, (uint16_t)(dstHeightOfs + dstHeight - 1) }; //xmin xmax ymin ymax
      RGA_set_dst_vir_info(&Rga_Request, handleDst.share_fd, reinterpret_cast<int>(dstLogical),  0, dstStride, dstHeightVir, &clip, dstFormat, 0);
    }


    // Go through all visible regions (clip rectangles?). 
    for ( int n = 0 ; n < (int)regionDst.numRects ; ) {
        // 16 rectangles a batch. 
        int m;
        hwc_rect_t        rectDsts[16];
        hwc_rect_t const* rects = regionDst.rects;

        for ( m = 0; n < (int)regionDst.numRects && m < 16; n++ ) {
            // Intersect clip with dest. 
          if ( intersect( rectDst, rects[n], rectDsts[m] ) == 0 )
                continue; // Skip if empty 

            // Advance to next rectangle. 
            m++;
        }

        // exit if no rects. 
        if (m == 0) {
            hwcONERROR(hwcSTATUS_INVALID_ARGUMENT);
        }

        for ( int i = 0 ; i < m ; i++ ) {
            unsigned int		Xoffset = rectDsts[i].left;
            unsigned int		Yoffset = rectDsts[i].top;
            unsigned int		WidthAct = rectDsts[i].right - rectDsts[i].left ;
            unsigned int		HeightAct = rectDsts[i].bottom - rectDsts[i].top ;

            RGA_set_dst_act_info(&Rga_Request, WidthAct, HeightAct, Xoffset, Yoffset + dstHeightOfs );
            HWC_LOG( ALOGD("@@@@ hwcClear w=%d h=%d s=%d ox=%d oy=%d dstMmuType=%d",
                           WidthAct, HeightAct, dstStride, Xoffset, Yoffset, dstMmuType) );

            if (ioctl(fdRGA, RGA_BLIT_ASYNC, &Rga_Request) != 0) {
                LOGE("%s(%d)[i=%d]:  RGA_BLIT_ASYNC Failed", __FUNCTION__, __LINE__, i);
            }
        }
    }

    return hwcSTATUS_OK;
OnError:
    LOGE("%s(%d):  Failed", __FUNCTION__, __LINE__);
    return status;
}
#endif

#if 0 // not used
static int rgaCopy(
    IN int                  fdRGA,
    IN struct private_handle_t const& handleSrc,
    IN struct private_handle_t const& handleDst
    )
{
    hwcSTATUS status = hwcSTATUS_OK;

    void *              srcLogical;
    unsigned int        srcStride;
    unsigned int        srcWidth;
    unsigned int        srcHeight;
    unsigned int        srcHeightVir;
    unsigned int        srcHeightOfs;
    RgaSURF_FORMAT      srcFormat;

    void *              dstLogical;
    unsigned int        dstStride;
    unsigned int        dstWidth;
    unsigned int        dstHeight;
    unsigned int        dstHeightVir;
    unsigned int        dstHeightOfs;
    RgaSURF_FORMAT      dstFormat;

    struct rga_req      Rga_Request;
    unsigned char       RotateMode = 0;
    int                 Rotation = 0;
    unsigned char       scale_mode = 0; //@@@@2;
    unsigned char       dither_en = 0;

    memset(&Rga_Request, 0x0, sizeof(Rga_Request));
    int srcMmuType = handleSrc.type ? 1 : 0;;
    int dstMmuType = handleDst.type ? 1 : 0;
    if (srcMmuType || dstMmuType) {
        RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
        Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (dstMmuType << 10) | (srcMmuType << 8);
    }

    rgaGetBufferInfo(
        handleSrc,
        &srcLogical,
        &srcWidth,
        &srcHeight,
        &srcStride,
        &srcHeightVir,
        &srcHeightOfs
        );

    rgaGetBufferInfo(
        handleDst,
        &dstLogical,
        &dstWidth,
        &dstHeight,
        &dstStride,
        &dstHeightVir,
        &dstHeightOfs
        );

    hwcONERROR( rgaFormat(handleSrc, &srcFormat ));
    hwcONERROR( rgaFormat(handleDst, &dstFormat ));
    {
        RECT clip = { 0, (uint16_t)(dstWidth - 1), (uint16_t)dstHeightOfs, (uint16_t)(dstHeightOfs + dstHeight - 1) }; //xmin xmax ymin ymax
        RGA_set_src_vir_info(&Rga_Request, handleSrc.share_fd, reinterpret_cast<int>(srcLogical),  0, srcStride, srcHeightVir, srcFormat, 0);
        RGA_set_dst_vir_info(&Rga_Request, handleDst.share_fd, reinterpret_cast<int>(dstLogical),  0, dstStride, dstHeightVir, &clip, dstFormat, 0);
    }
        
    RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);
    RGA_set_src_act_info(&Rga_Request, srcWidth, srcHeight, 0, srcHeightOfs );
    RGA_set_dst_act_info(&Rga_Request, dstWidth, dstHeight, 0, dstHeightOfs );
    //hwcONERROR( (hwcSTATUS)ioctl(fdRGA, RGA_BLIT_ASYNC, &Rga_Request) );
    hwcONERROR( (hwcSTATUS)ioctl(fdRGA, RGA_BLIT_SYNC, &Rga_Request) );

    return hwcSTATUS_OK;

  OnError:
    LOGE("%s(%d):  Failed", __FUNCTION__, __LINE__);
    /* Error roll back. */
    /* Unlock buffer. */
    return status;
}
#endif


//======================================
// layer info
//======================================
enum {
    // capability of overlay(WIN0,WIN1) and blitter(RGA)
    LCDC_WIN0   = 0x01,
    LCDC_WIN1   = 0x02,
    LCDC_RGA    = 0x04, // this means the layer can be used as blit source.


    // other layer info
    LAYER_HANDLE_CHANGED  = 0x08, // this include the case in which backgroundColor changed.
    LAYER_SKIP            = 0x10,
    LAYER_BACKGROUND      = 0x20,
    LAYER_DIM             = 0x40,
    LAYER_WIN1_WITH_RGBX  = 0x80, // this means that the pixel_format must be changed to RGBX(no alpha channel) from RGBA ,
                                  //   if the layer is mapped to win1.
    
    // mask
    //...lcdc
    LCDC_MASK_WIN = LCDC_WIN0 | LCDC_WIN1,
    LCDC_MASK_RGA = LCDC_RGA,
    LCDC_MASK_ALL = LCDC_MASK_WIN | LCDC_MASK_RGA,
    LCDC_NONE   = 0,
    //...layer
    LAYER_MASK_ALL        = LAYER_HANDLE_CHANGED | LAYER_SKIP | LAYER_BACKGROUND | LAYER_DIM | LAYER_WIN1_WITH_RGBX,
    //... all
    LAYER_INFO_MASK_ALL   = LCDC_MASK_ALL | LAYER_MASK_ALL,

    // bits
    LCDC_BITS_WIN = 2,
    LCDC_BITS_RGA = 1,
    LCDC_BITS_ALL = LCDC_BITS_WIN + LCDC_BITS_RGA, 
    LAYER_BITS    = 5,
    LAYER_INFO_BITS_ALL = LCDC_BITS_ALL + LAYER_BITS,
};

/*
  Calculate scale factor( src / disp ), and
  Determin supported LCDC functionality from the scale factor and the display area(src,disp), and
  return bitmap of LCDC_xxx

  note: 
  Scaled overlay is supported only by WIN0 ( not by WIN1 ).
  RGA(blitter) has no restriction with scale factor, 
  so return_value & LCDC_RGA != 0 (always).
*/
static int lcdcSupport( hwc_rect_t const& rectSrc, hwc_rect_t const& rectDisp, float& hFactor, float& vFactor )
{
    int   map = LAYER_INFO_MASK_ALL & ~LCDC_MASK_WIN;

    // w/h Src/Disp <--
    int const wSrc  = rectSrc.right   - rectSrc.left;
    int const hSrc  = rectSrc.bottom  - rectSrc.top;
    int const wDisp = rectDisp.right  - rectDisp.left;
    int const hDisp = rectDisp.bottom - rectDisp.top;


    // check size of displayFrame
    if ( wDisp <= 2 || hDisp <= 2 ) {
        HWC_LOG( ALOGW("zone is small ,LCDC can not support") );
        goto ret;
    }

    // check sizeof sourceCrop( not to consider bound of visibleRegionScreen. )
    if ( wSrc < 16 || hSrc < 16 ) {
        HWC_LOG(ALOGW("source is too small ,LCDC can not support"));
        goto ret;
    }
    //hwc_rect_t          rectClo;
    //hwc_rect_t          rectTmp = rectSrc;
    ////...rectTmp <-- adjust rectSrc
    //if ( closureRect( layer.visibleRegionScreen, rectClo ) > 0 ) {
    //  rectTmp.left   = std::max( rectTmp.left - (int)((rectDisp.left - rectClo.left) * hFactor), 0 );
    //  rectTmp.top    = std::max( rectTmp.top  - (int)((rectDisp.top  - rectClo.top ) * vFactor), 0 );
    //  rectTmp.right  = rectTmp.right  - (int)((rectDisp.right  - rectClo.right ) * hfactor);
    //  rectTmp.bottom = rectTmp.bottom - (int)((rectDisp.bottom - rectClo.bottom) * vhfactor);
    //}
    //if ( (rectTmp.right - rectTmp.left) <= 16 || (rectTmp.bottom - rectTmp.top) <= 16 ) {
    //		HWC_LOG(ALOGW("source is too small ,LCDC can not support"));
    //    goto ret;
    //}

    // check if no-scaling
    if ( wSrc == wDisp && hSrc == hDisp ) {
        hFactor = 1.0f;
        vFactor = 1.0f;
        map |= LCDC_WIN0 | LCDC_WIN1;
        goto ret;
    }

    // h/vFactor <--
    hFactor = (float)wSrc / wDisp;
    vFactor = (float)hSrc / hDisp;

    // check if scale factor is within the range.
    if ( hFactor <= 8 && hFactor >= 0.125 && vFactor <= 8 && vFactor >= 0.125 ) {
        // scaling is supported only by WIN0.
        map |= LCDC_WIN0;
    }

  ret:
    return map;
}


/*
  Determin supported LCDC function from the pixel format, and
  return bitmap of LCDC_xxx:

  WIN0/WIN1-supported pixel formats are determined from firefly's kernel source.
  ( But this is a little bit different from that declared in spec sheet,
  especially it comes to the chroma key order, CbCr CrCb etc.
  )

  RGA-supported pixel formats are determined from 'rk_hwc_com.cpp'( function hwcGetBufFormat() ).
  ( More formats could be supported 
  by adding Mapping from HAL_PIXEL_FORMAT_xxx to the kernel side format(RK_FORMAT_xxx)
  in the function 'hwcGetBufFormat' .
  )

*/
static int lcdcSupport( int pixelFormat )
{
    int   map = LAYER_INFO_MASK_ALL & ~LCDC_MASK_ALL;

    switch ( pixelFormat ) {
    case HAL_PIXEL_FORMAT_BGRA_8888:
    case HAL_PIXEL_FORMAT_RGBA_8888:
    case HAL_PIXEL_FORMAT_RGBX_8888:
    case HAL_PIXEL_FORMAT_RGB_565:
        map |= LCDC_WIN0 | LCDC_WIN1 | LCDC_RGA;
        break;
  
    case HAL_PIXEL_FORMAT_YCrCb_444:
    case HAL_PIXEL_FORMAT_YCbCr_422_SP:
        map |= LCDC_WIN1;
        break;

    case HAL_PIXEL_FORMAT_YCrCb_NV12:
        map |= LCDC_WIN0 | LCDC_RGA;
        break;

    case HAL_PIXEL_FORMAT_YCrCb_420_SP:
    case HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO:
        map |= LCDC_RGA;
        break;

    default:
        break;
    }

    HWC_LOG(ALOGD( "@@@@ lcdcSupport map=%x", map));
    return map;
}

/*
  Determin supported LCDC function from the layer attribute, and
  return bitmap of LCDC_xxx:

  hFactor,vFactor is set : if return_value != 0

  note: Overlay(WIN0, WIN1) does not support transform( rotation, flip etc ).

  note: 
    Currently RGA(blitter) does not work if transform == HWC_TRANSFORM_FLIP_V/H.
    ( This is not due to the h/w limitation, but due to the video driver implementation. )
    
  note about z_order:
    Member variable 'z_order' in struct rk_fb_win_par is not used on the video driver side,
    and the Z-order of win0/1(win_id=0/1) is fixed as below:
      win0 == bottom,
      win1 == top
    
  note about win0/1 blending:
    Alpha blending of the overlapped area of WIN0/1 is done as below:
      (case 1) When the pixel format of WIN1 has the alpha channel:
               the blending is done as if WIN1(src) were drawn onto WIN0(dest) with PREMULT.
      (case 2) When the pixel format of WIN1 does not have the alpha channel, 
               the blending is done as if WIN1 were drawn onto WIN0 with NO-BLENDING(ie copy).

*/

static int lcdcSupport( hwc_layer_1_t const& layer, float& hFactor, float& vFactor )
{
    hwc_rect_t const&   rectDisp = layer.displayFrame;
    hwc_rect_t const&   rectSrc = layer.sourceCrop;
    int                 bmapPixel=LAYER_INFO_MASK_ALL, bmapScale=LAYER_INFO_MASK_ALL, bmapOther=LCDC_MASK_ALL;

    // bmapOther <--
    //...check skip flag
    if ( layer.flags & HWC_SKIP_LAYER )
        bmapOther |= LAYER_SKIP;
    //...check HWC_BACKGROUND
    if ( layer.compositionType == HWC_BACKGROUND ) {
        bmapOther |= LAYER_BACKGROUND;
        bmapOther &= ~LCDC_MASK_WIN;  // to-do ( implement background color overlay )
        // in this case, handle, sourceCrop and transform are invalid, so skip to ret:.
        goto ret;
    }
    //...check handle
    if ( layer.handle == 0 ) {
        bmapOther &= ~LCDC_MASK_WIN;
        // this maybe a dimm layer.
        if ( layer.planeAlpha ) {
          bmapOther |= LAYER_DIM;
        }
        // in this case, handle, sourceCrop and transform are invalid, so skip to ret:.
        goto ret;
    }
    //...check transform
    switch ( layer.transform ) {
    case 0: // no transform
      break;
    case HWC_TRANSFORM_FLIP_H:
    case HWC_TRANSFORM_FLIP_V:
      bmapOther &= ~LCDC_MASK_RGA; // Currently RGA(blit) can not handle flip h/v, so exclude rga capability.
    default:
      bmapOther &= ~LCDC_MASK_WIN; // overlay(WIN0,WIN1) can not handle transform.
    }
    //...check blending
    switch ( layer.blending ) {
    case HWC_BLENDING_PREMULT:
        break;
    case HWC_BLENDING_COVERAGE:
        bmapOther &= ~LCDC_WIN1;
        break;
    case HWC_BLENDING_NONE:
    default:
        if ( privateHandle(layer.handle).format == HAL_PIXEL_FORMAT_RGBA_8888 ) {
            bmapOther |= LAYER_WIN1_WITH_RGBX;
        } else {
            bmapOther &= ~LCDC_WIN1;
        }
    }

    // bmapPixel <-- check pixel format
    bmapPixel = lcdcSupport( privateHandle(layer.handle).format );

    // h/vFactor <-- check scale factor
    bmapScale = lcdcSupport( rectSrc, rectDisp, hFactor, vFactor );


  ret:
    return bmapPixel & bmapScale & bmapOther;
}


//======================================
// class LayerGeometry
//======================================
// class LayerInfo
class LayerInfo {
public:
    enum {
        MAX_LAYERS = 32,
    };

    // modifier
    void        rebuild( hwc_display_contents_1_t const& disp );
    void        update( hwc_display_contents_1_t const& disp );

    // accessor
    int         needReDraw() const { return cntChanged_; }; // return count of the layer which needs re-draw.
    int         infoAND() const { return infoAND_; };
    int         infoOR() const { return infoOR_; };
    int         operator [](int index) const { return info_[index]; };

    // utility
    int         dump( char*& buff, int& len ) const;
private:
    int     info_[MAX_LAYERS];
    buffer_handle_t   handle_[MAX_LAYERS];  // this is backgroundColor if compositionType == HWC_BACKGROUND

    int     numLayer_;
    int     cntChanged_;  // count of the layer which handle changed.
    int     infoAND_;
    int     infoOR_;
};

void  LayerInfo::rebuild( hwc_display_contents_1_t const& disp )
{
    infoAND_ = LAYER_INFO_MASK_ALL;
    infoOR_  = 0;
    numLayer_ = disp.numHwLayers - 1;
    for ( int i = 0 ; i < numLayer_ ; i ++ ) {
        float       hFactor, vFactor;
        hwc_layer_1_t const&  layer = disp.hwLayers[i];
        int const             lcdc = lcdcSupport( layer, hFactor, vFactor ) | LAYER_HANDLE_CHANGED;
        
        info_[i] = lcdc;
        handle_[i] = layer.handle;

        infoAND_ &= lcdc;
        infoOR_ |= lcdc;
    }

    cntChanged_ = numLayer_;
}

void  LayerInfo::update( hwc_display_contents_1_t const& disp )
{
    infoAND_ = LAYER_INFO_MASK_ALL;
    infoOR_  = 0;
    cntChanged_ = 0;
    int const     numLayer = disp.numHwLayers - 1;
    for ( int i = 0 ; i < numLayer ; i ++ ) {
        hwc_layer_1_t const&  layer = disp.hwLayers[i];

        // in hwc_layer_1_t, handle overlaps with backgroundColor(hwc_color_t)
        if ( handle_[i] == layer.handle ) {
            info_[i] &= ~LAYER_HANDLE_CHANGED;
        } else {
            handle_[i] = layer.handle;
            info_[i] |= LAYER_HANDLE_CHANGED;
            cntChanged_ ++;
        }

        infoAND_ &= info_[i];
        infoOR_ |= info_[i];
    }
}

int   LayerInfo::dump( char*& buff, int& len ) const
{
    int   rc;

    DUMP( buff, len, "LayerInfo(changed=%d and=0x%x or=0x%x)\n", cntChanged_, infoAND_, infoOR_ );
    
    DUMP0( buff,len, "  W1X DIM BGR SKP HCG RGA Wn1 Wn0  handle\n" );
    for ( int i = 0 ; i < numLayer_ ; i ++ ) {
        DUMP0( buff,len, "   " );
        DUMPX( dumpBinary( buff, len, info_[i], LAYER_INFO_BITS_ALL, ' ', 'x', "   ", 1 ) );
        DUMP( buff, len, "   %p\n", handle_[i] );
    }

  ret:
    return rc;
}

// class LayerStack
class LayerStack {
public:
    enum {
        MAX_LAYERS = LayerInfo::MAX_LAYERS,
    };

    // modifier
    void      rebuild( hwc_display_contents_1_t const& disp );

    // accessor
    bool      hasOverlappedBelow( int iLayer ) const  { return overlap_[iLayer] & (((uint32_t)1<<iLayer)-1); };
    bool      hasOverlappedAvobe( int iLayer ) const  { return overlap_[iLayer] & (uint32_t)~1 << iLayer; };
    bool      overlap( int iLayer0, int iLayer1 ) const  { return overlap_[iLayer0] & (uint32_t)1 << iLayer1; };

    // utility
    int         dump( char*& buff, int& len ) const;
    
private:
    uint32_t    map() const { return (uint32_t)~0 >> (sizeof(uint32_t)*8 - numLayer_); };
    int         numLayer_;
    uint32_t    overlap_[MAX_LAYERS]; // overlap_[i].bits[k] == 1 if i-th layer and k-th layer overlap.
};

void  LayerStack::rebuild( hwc_display_contents_1_t const& disp )
{
    numLayer_ = disp.numHwLayers - 1;

    memset( overlap_, 0, sizeof(overlap_[0])*numLayer_ );

    hwc_layer_1_t const*  pL1 = disp.hwLayers;
    uint32_t              bmap1 = 1;
    for ( int i = 0 ; i < numLayer_ ; i++, bmap1 <<= 1, pL1 ++ ) {
        hwc_layer_1_t const&  l1 = *pL1;
        if ( l1.compositionType == HWC_BACKGROUND ) {
            // the layer(HWC_BACKGROUND) overlaps with all others.
            overlap_[i] = map();
            for ( int k = 0 ; k < numLayer_ ; k ++ ) {
                overlap_[k] |= bmap1;
            }
            continue;
        }
        hwc_layer_1_t const*  pL2 = pL1 + 1;
        uint32_t              bmap2 = bmap1 << 1;
        for ( int k = i + 1 ; k < numLayer_ ; k ++, bmap2 <<= 1, pL2 ++ ) {
            hwc_layer_1_t const&  l2 = *pL2;
            if ( intersect( l1.displayFrame, l2.displayFrame ) &&
                 intersect( l1.visibleRegionScreen, l2.visibleRegionScreen ) ) {
                overlap_[k] |= bmap1;
                overlap_[i] |= bmap2;
            }
        }
    }
}

int   LayerStack::dump( char*& buff, int& len ) const
{
    int   rc;

    DUMP0( buff, len, "LayerStack\n" );
    
    for ( int i = 0 ; i < numLayer_ ; i ++ ) {
        DUMP0( buff, len, "  --" );
        DUMPX( dumpBinary( buff, len, overlap_[i], numLayer_, '.', 'x', " " ) );
        DUMP0( buff, len, "--\n" );
    }
    DUMP0( buff, len, "\n" );

  ret:
    return rc;
}

// class LayerGeometry
/*
  LayerGeometry::update should be called at the beginning of hwc_prepare_primary()
 */
class LayerGeometry {
public:
    enum {
        MAX_LAYERS = LayerInfo::MAX_LAYERS,
    };

    // constructor
    LayerGeometry()   { reset(); };

    // modifier
    void              reset() { numLayer_ = -1; cntSame_ = 0; };
    void              update( hwc_display_contents_1_t const& disp );

    // accessor
    int               numLayer() const      { return numLayer_; };
    LayerInfo const&  layerInfo() const     { return info_; };
    LayerStack const& layerStack() const    { return lstack_; };
    bool              changed() const       { return cntSame_ < 2; };  // true if the geometry changed.

    // utility
    int         dump( char*& buff, int& len ) const;

private:
    int         numLayer_;  // count of the layer excluding last FB target, == -1 if after reset
    LayerInfo   info_;
    LayerStack  lstack_;
    int         cntSame_;   // when at least 3 frames are same ==> the geometry is not changed.
};

void  LayerGeometry::update( hwc_display_contents_1_t const& disp )
{
    if ( disp.numHwLayers > MAX_LAYERS + 1 ) {// '+1' means excluding last frame buffer target.
        reset();
        return;
    }

    if ( disp.flags & HWC_GEOMETRY_CHANGED || (int)disp.numHwLayers != numLayer_ + 1 ) {
        cntSame_ = 0;
        numLayer_ = disp.numHwLayers -1;
        info_.rebuild( disp );
        lstack_.rebuild( disp );
    } else {
        (++ cntSame_ ) &= 0xff; // prevent overflow
        info_.update( disp );
    }
}

int   LayerGeometry::dump( char*& buff, int& len ) const
{
    int   rc;

    DUMP( buff, len, "LayerGeometry cntSame=%d\n\n", cntSame_ );
    DUMPX( info_.dump( buff, len ) );
    DUMP0( buff, len, "\n" );
    DUMPX( lstack_.dump( buff, len ) );

  ret:
    return rc;
}


    

//======================================
// try_xxx_policy and set_xxx_policy
//======================================
/*
  This contains infomation to configure the overlay and blitter h/w.
  The infomation is 
    (1) gathered at try_xxx_policy(), and
    (2) used     at set_xxx_policy().

  'invalidate()' should be called at 
    (1) at the beginning of the boot,
    (2) at hwc_blank(,HWC_DISPLAY_PRIMARY,).

  note:
    Usually calling 'hwc_prepare()' and 'hwc_set()' are paired 
    (ie hwc_prepare is called first, and hwc_set is next ).
    But sometimes(especially boot time), hwc_set() is called without calling hwc_prepare() beforehand.
    Such a case can be detected by using 'isPrepared()', and should be handled specially.

 */
static struct PreparedInfo {
    // constructor
    PreparedInfo();

    // modifier
    void      invalidate();
    void      preparePolicy( policy_entry const& policy );  // should be called at hwc_prepare()
    void      prepareOverlay( int iOvl )  { iOverlayPrepared = iOvl; };
    void      prepareRedraw( bool redraw )  { redrawPrepared = redraw; };
    policy_entry const& executePolicy();  // should be called at hwc_set()

    // accessor
    bool      isPrepared() const { return isValidPolicy( *pPolicyPrepared ); };
    bool      policyDiffers( TRY_POLICY_FNC ) const;

    // ulility
    int       dump( char*&buff, int& len ) const;

    // member variable
    //...common
    policy_entry const*   pPolicyPrepared;
    policy_entry const*   pPolicyLast;    // latest executed policy
    int                   iOverlayLast, iOverlayPrepared; // index of the overlayed layer
    int                   numWin;
    struct win_ {
        int               idWin;
        int               zOrder;
        hwc_layer_1_t*    pLayer;
    } win[VOP_WIN_NUM];
    //...vop_rga_policy specific
    hwc_layer_1_t         layerRGA;
    bool                  needReDraw;
    int                   fence[FB_BUFFERS_NUM];
    //...vop_gpu_reuse_policy specific
    bool                  redrawPrepared;
    bool                  redrawLast;
} prepInfo_g;

PreparedInfo::PreparedInfo()
{
    invalidate();
    layerRGA.releaseFenceFd = -1;
    for ( int i = 0 ; i < NELEM(fence) ; i ++ )
        fence[i] = -1;
}

void  PreparedInfo::invalidate()
{
    pPolicyPrepared = pPolicyLast = 0;
    iOverlayPrepared = iOverlayLast = -1;
    redrawPrepared = redrawLast = false;
}

void  PreparedInfo::preparePolicy( policy_entry const& policy )
{
    pPolicyPrepared = &policy;
}
    
policy_entry const& PreparedInfo::executePolicy()
{
    iOverlayLast = iOverlayPrepared;
    iOverlayPrepared = -1;

    pPolicyLast = pPolicyPrepared;
    pPolicyPrepared = 0;

    redrawLast = redrawPrepared;
    redrawPrepared = false;

    return isValidPolicy( *pPolicyLast ) ? *pPolicyLast : *(policy_entry const*)0;
}

bool  PreparedInfo::policyDiffers( TRY_POLICY_FNC fnc ) const
{
    return
        pPolicyLast == 0 ||
        pPolicyLast->try_policy_fnc != fnc;
}

int PreparedInfo::dump( char*&buff, int& len ) const
{
    int   rc = 0;
    char const*     strPolicyPrep = isValidPolicy( *pPolicyPrepared ) ? pPolicyPrepared->strCompose : "invalid";
    char const*     strPolicyLast = isValidPolicy( *pPolicyLast ) ? pPolicyLast->strCompose : "invalid";
    char const*     strRedraw = needReDraw ? "redraw" : "reuse";

    DUMP( buff, len, "prepInfo_g policy=%s,%s, numW=%d draw=%s iOvl=%d,%d redraw=%d,%d\n",
          strPolicyPrep, strPolicyLast, numWin, strRedraw, iOverlayPrepared, iOverlayLast, (int)redrawPrepared, (int)redrawLast );
    for ( int i = 0 ; i < numWin ; i ++ ) {
        win_ const&           w = win[i];
        hwc_layer_1_t const&  layer = *w.pLayer;
        if ( &layer )
            DUMP( buff, len, "  id=%d z=%d, h=%p\n", w.idWin, w.zOrder, layer.handle );
    }

  ret:
    return rc;
}



/*
  The flowing member variables are not used on the kernel driver side.
  fbdc_en, fbdc_cor_en, fbdc_data_format, data_space


  Fields of hwc_layer_1_t used are:
  sourceCrop
  displayFrame
  handle (format, share_fd, height, offset, size, stride )
  acquireFenceFd

  note:
    According to the firefly's kernel source:
    (1) member variable 'alpha_mode' and 'g_alpha_val' in struct rk_fb_win_par seems to be ignored.
    (2) to enable alpha or not is determined from pixel format.
    (3) g_alpha_val might be plane alpha, but g_alpha_val is not used.
*/

void  setWin( struct rk_fb_win_par& win, hwc_layer_1_t const& layer, int win_id, int z_order, int formatOverride )
{
    struct private_handle_t const&  handle = privateHandle( layer.handle );

    /*
      area_par[].xxx
      xpos,ypos,xsize,ysize -- destination(screen) rect
      x_offset,y_offset,xact,yact -- source rect ( which is mapped into 'destination rect' )
      xvir,yvir -- source size ( which contains 'source rect' )
      ion_fd, data_format -- source fd and format
      alpha_mode;
      ( in hardware/rk_fh.h -- AB_CLEAR, AB_SRC, AB_DST, AB_SRC_OVER, AB_DST_OVER etc )
      g_alpha_val;
    */

    hwc_rect_t const& rectSrc = layer.sourceCrop;
    hwc_rect_t const& rectDst = layer.displayFrame;

    win.win_id = win_id;
    win.z_order = z_order;

    if ( formatOverride >= 0 )
        win.area_par[0].data_format = formatOverride;
    else  
        win.area_par[0].data_format = handle.format;
    win.area_par[0].ion_fd = handle.share_fd;
    win.area_par[0].acq_fence_fd = layer.acquireFenceFd;
    //...destination
    win.area_par[0].xpos = rectDst.left;
    win.area_par[0].ypos = rectDst.top;
    win.area_par[0].xsize = rectDst.right - rectDst.left;
    win.area_par[0].ysize = rectDst.bottom -  rectDst.top;
    //...source
    win.area_par[0].x_offset = rectSrc.left;
    win.area_par[0].xact = rectSrc.right - rectSrc.left;
    win.area_par[0].yact = rectSrc.bottom - rectSrc.top;
    // to-do( limit rectDist to fit within the frame buffer )
    // to-do( test various cases of the yuv formats )
    uint32_t  wVir, hVir, yOfs;
    getVirtual( handle, wVir, hVir, yOfs );
    win.area_par[0].xvir = wVir;
    win.area_par[0].yvir = hVir;
    win.area_par[0].y_offset = yOfs + rectSrc.top;


#if 0
    DEBUG_CODE(
               if (win_id==1) {
                 //alpha
                 static int      amode = 0;
                 static uint16_t galpha = 0;
                 int   tmp =  hwc_get_int_property("sys.amode","-1");
                 if ( tmp >= 0 && tmp != amode ) {
                   ALOGD("@@@@ amode=%d", tmp );
                   amode = tmp;
                 }
                 tmp =  hwc_get_int_property("sys.galpha","-1");
                 if ( tmp >= 0 && tmp != galpha ) {
                   ALOGD("@@@@ galpha=0x%x", tmp );
                   galpha= tmp;
                 }
                 win.alpha_mode = amode;
                 win.g_alpha_val = galpha;
               }
               )
#endif    
}


/*
  This policy assumes:
  there are 2 or less layers excluding last layer(==frame buffer target).
  all layers can be overlayed using WIN0 and WIN1.
*/
static int try_vop_policy( hwcContext& /*context*/, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    int const numLayer = disp.numHwLayers - 1;
    if ( numLayer > VOP_WIN_NUM || numLayer == 0 ) {
        HWC_LOG(ALOGD("line=%d,num=%d",__LINE__, numLayer))
            return -1;
    }

    LayerInfo const&  layerInfo = geo.layerInfo();
    int   cntFrameBuffer = 0;
    prepInfo_g.numWin = numLayer;
    for ( int i = 0 ; i < numLayer ; i ++ ) {
        int const   lToW[2] = { LCDC_WIN0, LCDC_WIN1 };
        hwc_layer_1_t&  layer = disp.hwLayers[i];
        int const       linfo = layerInfo[i];

        if ( linfo & lToW[i] ) {
            prepInfo_g.win[i].pLayer = &layer;
            layer.compositionType = HWC_OVERLAY;
        } else {
            //prepInfo_g.win[i].pLayer = &disp.hwLayers[numLayer]; // framebuffer target
            //layer.compositionType = HWC_FRAMEBUFFER;
            cntFrameBuffer ++;
        }
        prepInfo_g.win[i].idWin  = i;
        prepInfo_g.win[i].zOrder = i;
    }

    //if ( numLayer == cntFrameBuffer ) { // None of the layers is overlayed.
    if ( cntFrameBuffer ) {
        return -1;
    }

    return 0;
}

static int set_vop_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& )
{
    int const   numLayer = disp.numHwLayers - 1;

    // fb_info <--
    struct rk_fb_win_cfg_data fb_info ;
    //...clear
    memset(&fb_info, 0, sizeof(fb_info));
    fb_info.ret_fence_fd = -1;
    for ( int i = 0 ; i < VOP_WIN_NUM ; i++ ) {
        fb_info.rel_fence_fd[i] = -1;
    }
    //...fb_info.win_par[] <--
    for ( int i = 0 ; i < prepInfo_g.numWin ; i ++ ) {
        hwc_layer_1_t&  layer = *prepInfo_g.win[i].pLayer;
        int const       idWin = prepInfo_g.win[i].idWin;
        int const       zOrder = prepInfo_g.win[i].zOrder;
        int const       format = (idWin == 1 && layer.blending==HWC_BLENDING_NONE) ? HAL_PIXEL_FORMAT_RGBX_8888 : -1;
        setWin( fb_info.win_par[i], layer, idWin, zOrder, format );
    }

    // ioctl <-- fb_info
    if ( ioctl( context.fbFd, RK_FBIOSET_CONFIG_DONE, &fb_info ) ) {
        ALOGE("@@@@ioctl config done error");
        return -1;
    }

    // layer[].acquireFenceFd <-- close
    // layer[].releaseFenceFd <-- fb_info
    for ( int i = 0 ; i <= numLayer ; i ++ ) {  // '<=' means including FB target.
        hwc_layer_1_t&  layer = disp.hwLayers[i];
        layer.releaseFenceFd = -1;
        if ( layer.acquireFenceFd > 0 ) {
            close( layer.acquireFenceFd );
            layer.acquireFenceFd = -1;
        }
    }
    for ( int i = 0 ; i < prepInfo_g.numWin ; i ++ ) {
        if ( fb_info.rel_fence_fd[i] > -1 ) {
            hwc_layer_1_t&    layer = *prepInfo_g.win[i].pLayer;

            DEBUG_CODE(
                size_t const  iLayer = ((uintptr_t)&layer - (uintptr_t)disp.hwLayers) / sizeof(hwc_layer_1_t);
                size_t const  iLayer2 = &layer - disp.hwLayers;
                       
                if ( &layer != &prepInfo_g.layerRGA && iLayer > (size_t)numLayer ) {
                    ALOGE("@@@@ logical error: %s %d : %d %d %d %d", __FUNCTION__, __LINE__, i, numLayer, iLayer, iLayer2 );
                    goto ret;
                }
                )

                layer.releaseFenceFd = fb_info.rel_fence_fd[i];

        } else {
            // this happens when the display is blanked.
            if ( ! context.fb_blanked )
                ALOGD("@@@@ rel_fence is not available: %s(%d) i=%d", __FUNCTION__, __LINE__, i);
        }
    }

    DEBUG_CODE(
               for ( int i = prepInfo_g.numWin ; i < RK_MAX_BUF_NUM ; i ++ ) {
                 if ( fb_info.rel_fence_fd[i] > -1 ) {
                     ALOGE("@@@@ logical error: %s %d : %d %d", __FUNCTION__, __LINE__, i, fb_info.rel_fence_fd[i]);
                     close( fb_info.rel_fence_fd[i] );
                 }
               }
               )
  
    // disp.retireFenceFd <--
    if ( fb_info.ret_fence_fd > -1 )
        disp.retireFenceFd = fb_info.ret_fence_fd;      

  ret:
    return 0;
}

/*
  This policy assumes:
  At least one layer can be handled by overlay.
  Other layers can be handled by blitter.


  Action( on set_xx_policy() side ):
  one layer( to be overlayed ) --> winx, 
  (case 1) if other layers need to be re-drawn ,
           --> allocate buffer
           --> draw these layers onto the buffer
           --> the buffer -->winx
  (case 2) if other layers are same as previous video frame( ie no-need to draw ) AND
              these layers are already blitted onto the buffer,
           --> the buffer -->winx

*/
#if USE_VOP_RGA_POLICY // vop_gpu_reuse_policy covers most of the vop_rga_policy's functionality.
static int try_vop_rga_policy( hwcContext&context, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    int const         numLayer = geo.numLayer();
    LayerInfo const&  layerInfo  = geo.layerInfo();
    LayerStack const& layerStack = geo.layerStack();

    // currently DIM and BACKGROUND layer handling is incomplete, so exclude the case.
    if ( ( layerInfo.infoAND() & LCDC_MASK_ALL ) == 0 ||
         ( layerInfo.infoOR() & (LAYER_SKIP | LAYER_BACKGROUND | LAYER_DIM ) )
        )
        return -1;

    // find a layer to be overlayed.
    int   maxArea = 0;
    int   iOverlay = -1;
    bool  isBottom = true;
    //...scan from bottom
    for ( int i = 0 ; i < numLayer ; i ++ ) {
        if ( layerStack.hasOverlappedBelow( i ) ) {
            continue;
        }
        hwc_layer_1_t const&  layer = disp.hwLayers[i];
        int const             linfo = layerInfo[i];
        if ( linfo & LCDC_WIN0 ) {
            int const   weight = linfo & LAYER_HANDLE_CHANGED ? 2 : 0;
            int         ar = area( layer.visibleRegionScreen ) << weight;
            if ( maxArea < ar ) {
                int   iOvlCandidate = i;
                for ( int k = i ; ++ k < numLayer ; ) {
                    hwc_layer_1_t const&  layerK = disp.hwLayers[k];
                    if ( ! layerStack.overlap( i, k ) &&
                         ! intersect( layer.displayFrame, layerK.displayFrame ) )
                        continue;

                    for ( int m = k ; ++ m < numLayer ; ) {
                        if ( ! layerStack.overlap( k, m ) )
                            continue;

                        hwc_layer_1_t const&  layerM = disp.hwLayers[m];
                        if ( layerM.blending == HWC_BLENDING_NONE )
                            continue;

                        hwc_rect_t            rectKM;
                        intersect( layerK.displayFrame, layerM.displayFrame, rectKM );
                        if ( ! intersect( layer.displayFrame, rectKM ) )
                            continue;

                        // in this case,
                        //   3 layers[i<k<m] overlaps, and
                        //   layer[m] needs alpha blending when it is blitted, and
                        //   layer[k]'s alpha channel is cleared after blitting.
                        // So skip this 'iOvlCandidate' and search next candidate.
                        iOvlCandidate = iOverlay;
                        ar = maxArea;
                        k = numLayer;
                        break;
                    }
                }

                iOverlay = iOvlCandidate;
                maxArea = ar;
            }
        }
    }
    //...scan from top
    for ( int i = numLayer ; -- i >= 0 ; ) {
        if ( layerStack.hasOverlappedAvobe( i ) )
            continue;
        hwc_layer_1_t const&  layer = disp.hwLayers[i];
        int const             linfo = layerInfo[i];
        if ( linfo & LCDC_WIN1 && layer.visibleRegionScreen.numRects == 1 ) {
            int const weight = linfo & LAYER_HANDLE_CHANGED ? 2 : 0;
            int const ar = area( layer.visibleRegionScreen ) << weight;
            if ( maxArea < ar ) {
                iOverlay = i;
                maxArea = ar;
                isBottom = false;
            }
        }
    }
    if ( iOverlay < 0 ) {
        return -1;
    }

    // rectBound <-- blitter destination
    // disp.hwLayers[].compositionType <--
    hwc_rect_t  rectBound = { 0,0,0,0 };
    for ( int i = 0; i < numLayer ; i++ ) {
        hwc_layer_1_t&  layer = disp.hwLayers[i];

        if ( i == iOverlay ) {
            layer.compositionType = HWC_OVERLAY;
            continue;
        }
        if ( layerInfo[i] & LAYER_BACKGROUND ) {
            layer.compositionType = HWC_BACKGROUND;
            rectBound.right = context.fbhandle.width;
            rectBound.bottom = context.fbhandle.height;
            continue;
        }
        if ( layerInfo[i] & LAYER_DIM ) {
            layer.compositionType = HWC_DIM;
        } else {
            DEBUG_CODE(
                if ( layer.compositionType != HWC_FRAMEBUFFER &&
                     layer.compositionType != HWC_BLITTER &&
                     layer.compositionType != HWC_OVERLAY )
                    ALOGD("@@@@ %s(%d) type=%d", __FUNCTION__,__LINE__, layer.compositionType );
                );
            layer.compositionType = HWC_BLITTER;
        }
        bound( layer.displayFrame, rectBound, rectBound );
    }

    // needReDraw <--
    bool  needReDraw = true;
    do {
        if ( prepInfo_g.numWin == 1 ) {
            needReDraw = false;
            break;
        }
        if ( geo.changed() || geo.layerInfo().needReDraw() >= 2 )
            break;
        if ( geo.layerInfo().needReDraw() == 1 && (layerInfo[iOverlay] & LAYER_HANDLE_CHANGED) == 0 )
            break;
        if ( prepInfo_g.iOverlayLast != iOverlay )
            break;
        if ( prepInfo_g.policyDiffers( try_vop_rga_policy ) )
            break;
        needReDraw = false;
    } while (false);


    // prepInfo_g <-- save info into prepInfo_g to call set_xx() later.
    prepInfo_g.needReDraw = needReDraw;
    prepInfo_g.numWin = 1;
    prepInfo_g.win[0].idWin = prepInfo_g.win[0].zOrder = isBottom ? 0 : 1;
    prepInfo_g.win[0].pLayer = &disp.hwLayers[iOverlay];
    prepInfo_g.prepareOverlay( iOverlay );
    if ( ! isEmpty( rectBound ) ) {
        //...prepInfo_g.layerRGA <--
        hwc_layer_1_t&            layerRGA = prepInfo_g.layerRGA;
        layerRGA.displayFrame = layerRGA.sourceCrop = rectBound;
        layerRGA.blending = HWC_BLENDING_PREMULT;
        layerRGA.handle = context.phd_bk[context.membk_index];
        layerRGA.visibleRegionScreen.numRects = 1;
        layerRGA.visibleRegionScreen.rects = &layerRGA.displayFrame;
        layerRGA.releaseFenceFd = layerRGA.acquireFenceFd = -1;
        //...prepInfo_g.win[] <--
        prepInfo_g.numWin = 2;
        prepInfo_g.win[1].idWin = prepInfo_g.win[1].zOrder = prepInfo_g.win[0].idWin ^ 1;
        prepInfo_g.win[1].pLayer = &prepInfo_g.layerRGA;
    }

    return 0;
}

static int set_vop_rga_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    int             rc, errLine;
    hwc_layer_1_t&  layerRGA = prepInfo_g.layerRGA;

    // blit
    if ( prepInfo_g.needReDraw ) {
        // blit into the newly allocated buffer.
        //...allocate a buffer to be the blitter target.
        if ( ++ context.membk_index >= FB_BUFFERS_NUM )
            context.membk_index = 0;
        int const membk_index = context.membk_index;
        if ( prepInfo_g.fence[membk_index] >= 0 ) {
            sync_wait( prepInfo_g.fence[membk_index], 500 );
            close( prepInfo_g.fence[membk_index] );
            prepInfo_g.fence[membk_index] = -1;
        }
        buffer_handle_t const handle = context.phd_bk[membk_index];
        struct private_handle_t const&  handleRGA = privateHandle(handle);
        //...replace layerRGA.handle(previously blitted) with the newly allocated.
        layerRGA.handle = handle;
        //...clear if the target layer of blitter(win[1]) covers another layer(win[0]).
        int const   isLayerRgaTop = prepInfo_g.win[1].zOrder;
        hwc_region_t const  regionRGA = { 1, &layerRGA.displayFrame };
        if ( isLayerRgaTop ) {
            rc = rgaClear( context.engine_fd, 0x00000000,
                           HWC_BLENDING_NONE,
                           handleRGA,
                           layerRGA.displayFrame,
                           regionRGA );
            if ( rc ) { errLine = __LINE__; goto ret; }
        }
        //...blit
        int const   numLayer = disp.numHwLayers - 1;
        for ( int i = 0; i < numLayer ; i++ ) {
            hwc_layer_1_t const&  layer = disp.hwLayers[i];
            union {
                hwc_color bgColor;
                int32_t   iColor;
            };
            rc = 0;
            switch ( layer.compositionType ) {
            case HWC_BACKGROUND:
                bgColor = layer.backgroundColor;
                rc = rgaClear( context.engine_fd, iColor,
                               HWC_BLENDING_NONE,
                               handleRGA,
                               layerRGA.displayFrame,
                               regionRGA );
                break;
            case HWC_DIM:
                rc = rgaClear( context.engine_fd, (uint32_t)layer.planeAlpha << 24,
                               layer.blending,
                               handleRGA,
                               layer.displayFrame,
                               layer.visibleRegionScreen );

            case HWC_BLITTER:
                if (layer.acquireFenceFd > 0) {
                    sync_wait(layer.acquireFenceFd, 500);
                    //close( layer.acquireFenceFd ); closing is done at set_vop_policy()
                }
                rc = rgaBlit(context.engine_fd, layer, handleRGA, i);
                break;
            }
            if ( rc ) { errLine = __LINE__; goto ret; }
        }

        rc = ioctl(context.engine_fd, RGA_FLUSH, NULL);
        if ( rc ) { errLine = __LINE__; goto ret; }
    }

    // overlay
    rc = set_vop_policy( context, disp, geo );
    //...to-do ( pairing context.phd_bk[] and layerRGA.releaseFenceFd )
    if ( layerRGA.releaseFenceFd > 0 ) {
        if ( prepInfo_g.fence[context.membk_index] >= 0 )
            close( prepInfo_g.fence[context.membk_index] );
        prepInfo_g.fence[context.membk_index] = layerRGA.releaseFenceFd;
        layerRGA.releaseFenceFd = -1;
    }
    if ( rc ) { errLine = __LINE__; goto ret; }

    rc = 0;
  ret:
    if ( rc ) {
        ALOGE("@@@@ %s(%d)", __FUNCTION__, errLine );
    }
    return rc;
}
#endif

/*
  This policy assumes:
  At least one layer can be handled by overlay.
  Other layers can be handled by gpu.

  Action( on set_xx_policy() side ):
  one layer( suits for overlay ) -> winx, 
  other layers -gpu-> winx
*/
#if 0 // vop_gpu_reuse_policy supersedes vop_gpu_policy.
static int try_vop_gpu_policy( hwcContext&, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    if ( ( geo.layerInfo().infoOR() & LCDC_MASK_WIN ) == 0 )
        return -1;

    int const   numLayer = geo.numLayer();
  
    // find a layer to be overlayed.
    LayerInfo const&  layerInfo  = geo.layerInfo();
    LayerStack const& layerStack = geo.layerStack();
    int   maxArea = 0;
    int   iOverlay = -1;
    bool  isBottom = true;
    //...scan from bottom
    for ( int i = 0 ; i < numLayer ; i ++ ) {
        if ( layerStack.hasOverlappedBelow( i ) )
            continue;

        hwc_layer_1_t const&  layer = disp.hwLayers[i];
        int const             linfo = layerInfo[i];
        if ( linfo & LCDC_WIN0 ) {
            int const ar = area( layer.visibleRegionScreen );
            if ( maxArea < ar ) {
                iOverlay = i;
                maxArea = ar;
            }
        }
    }
    //...scan from top
    for ( int i = numLayer ; -- i >= 0 ; ) {
        if ( layerStack.hasOverlappedAvobe( i ) )
            continue;
        hwc_layer_1_t const&  layer = disp.hwLayers[i];
        int const             linfo = layerInfo[i];
        if ( linfo & LCDC_WIN1 ) {
            int const ar = area( layer.visibleRegionScreen );
            if ( maxArea < ar ) {
                iOverlay = i;
                maxArea = ar;
                isBottom = false;
            }
        }
    }
    if ( iOverlay < 0 ) 
        return -1;

    // disp.hwLayers[].compositionType <--
    for ( int i = 0; i < numLayer ; i++ ) {
        disp.hwLayers[i].compositionType = HWC_FRAMEBUFFER;
    }
    disp.hwLayers[iOverlay].compositionType = HWC_OVERLAY;


    // prepInfo_g <-- save info to call set_xxx() later
    prepInfo_g.win[0].idWin = isBottom ? 0 : 1;
    prepInfo_g.win[0].zOrder = isBottom ? 0 : 1;
    prepInfo_g.win[0].pLayer = &disp.hwLayers[iOverlay];
    prepInfo_g.win[1].idWin = prepInfo_g.win[0].idWin ^ 1;
    prepInfo_g.win[1].zOrder = prepInfo_g.win[0].zOrder ^ 1;
    prepInfo_g.win[1].pLayer = &disp.hwLayers[numLayer];  // HWC_FRAMEBUFFER_TARGET
    prepInfo_g.numWin = 2;

    return 0;
}

static int set_vop_gpu_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    return set_vop_policy( context, disp, geo );
}
#endif


/*
  This policy assumes:
    At least one layer can be handled by overlay.
    Other layers can be handled by gpu(SurfaceFlinger).
    SurfaceFlinger does double-buffering((or triple) for gpu-drawing.   

  Action( on set_xx_policy() side ):
  one layer( suits for overlay ) -> winx, 
  (case 1) if other layers need to be re-drawn ,
           --> map current frame's FB-target-->winx
           --> draw these layers onto the next frame's FB-target with gpu 
  (case 2) if other layers are same as previous video frame( ie no-need to draw ) AND
              the previous frame is already drawn and available as current frame's FB-target,
           --> map current frame's FB-target -->winx
           --> stop drawing with gpu

  note:
    In this policy, layers of the current and previous frame are mixed like follows:
    (1) one layer( suits for overlay ) -- of current frame
    (2) other layers( drawn with gpu ) -- of previous frame
    So sometimes visual glitch may happen.


  note about the FB-target's buffer handle:
    ( In the following description, 
      'FBT-layer'  means disp.hwLayers[disp.numHwLayers-1],
      'FBT-handle' means FBT-layer.handle, and
      'GPU-layer'  means disp.hwLayers[0..disp.numHwLayers-2] which compositionType==HWC_FRAMEBUFFER.
    )

    At the point of executing hwc_set(),
    FBT-handle indicates a buffer to be shown at the next frame,
    It's not a buffer onto which GPU-layers are drawn next.
    ( Assuming SurfaceFlinger does double-buffering((or triple) for gpu-drawing. )

    This means that:
      After hwc_set() returns,
      while the contents of the FBT-handle is shown to the user during next frame,
      Surfaceflinger draws each GPU-layer onto the newly allocated(actually recycled) buffer 
      other than FB-handle with GPU.
      And the newly allocated buffer's handle appears as FB-handle 
      at the next call of hwc_prepare() or hwc_set().

    So to reuse the result of GPU drawing, it's necessary to use the FBT-handle 
    at the next call of hwc_prepare() or hwc_set().

    example of calling sequence:
      SurfaceFlinger calls hwc_set( GPU-layer0(current-frame), GPU-layer1(current-frame), ... FBT-layer(current-frame) )
      --> hwcomposer makes FBT-layer(current-frame) visible.
      <-- return to SurfaceFlinger
        
      SurfaceFlinger draws GPU-layer0,1...(current-frame) onto FBT-layer(next-frame) with GPU.

      SurfaceFlinger call of hwc_set(  ... FBT-layer(next-frame) )
      --> hwcomposer makes FBT-layer(next-frame) visible.
*/

static int try_vop_gpu_reuse_policy( hwcContext&, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    int const         numLayer = geo.numLayer();
    LayerInfo const&  layerInfo  = geo.layerInfo();
    LayerStack const& layerStack = geo.layerStack();


    // currently DIM and BACKGROUND layer handling is incomplete, so exclude the case.
    if ( ( layerInfo.infoAND() & LCDC_MASK_ALL ) == 0 ||
         ( layerInfo.infoOR() & (LAYER_SKIP | LAYER_BACKGROUND | LAYER_DIM ) )
        )
        return -1;
  

    // find a layer to be overlayed.
    int   maxArea = 0;
    int   iOverlay = -1;
    bool  isBottom = true;
    //...scan from bottom
    for ( int i = 0 ; i < numLayer ; i ++ ) {
        if ( layerStack.hasOverlappedBelow( i ) ) {
            continue;
        }
        hwc_layer_1_t const&  layer = disp.hwLayers[i];
        int const             linfo = layerInfo[i];
        if ( linfo & LCDC_WIN0 ) {
            int const weight = linfo & LAYER_HANDLE_CHANGED ? 2 : 0;
            int const ar = area( layer.visibleRegionScreen ) << weight;
            if ( maxArea < ar ) {
                iOverlay = i;
                maxArea = ar;
            }
        }
    }
    //...scan from top
    for ( int i = numLayer ; -- i >= 0 ; ) {
        if ( layerStack.hasOverlappedAvobe( i ) )
            continue;
        hwc_layer_1_t const&  layer = disp.hwLayers[i];
        int const             linfo = layerInfo[i];
        if ( linfo & LCDC_WIN1 && layer.visibleRegionScreen.numRects == 1 ) {
            int const weight = linfo & LAYER_HANDLE_CHANGED ? 2 : 0;
            int const ar = area( layer.visibleRegionScreen ) << weight;
            if ( maxArea < ar ) {
                iOverlay = i;
                maxArea = ar;
                isBottom = false;
            }
        }
    }
    if ( iOverlay < 0 ) {
        return -1;
    }

    // needReDraw <--
    bool  needReDraw = true;
    do {
        if ( numLayer == 1 ) {
            needReDraw = false;
            break;
        }
        if ( geo.changed() || geo.layerInfo().needReDraw() >= 2 )
            break;
        if ( geo.layerInfo().needReDraw() == 1 && (layerInfo[iOverlay] & LAYER_HANDLE_CHANGED) == 0 )
            break;
        if ( prepInfo_g.iOverlayLast != iOverlay )
            break;
        if ( prepInfo_g.policyDiffers( try_vop_gpu_reuse_policy ) )
            break;
        needReDraw = false;
    } while (false);

    // disp.hwLayers[].compositionType <--
    int const     compositionType = needReDraw ? HWC_FRAMEBUFFER : HWC_NODRAW;
    for ( int i = 0; i < numLayer ; i++ ) {
        hwc_layer_1_t&  layer = disp.hwLayers[i];
        if ( ( layerInfo[i] & LAYER_BACKGROUND ) == 0 ) 
            layer.compositionType = compositionType;
    }
    hwc_layer_1_t&  layerOvl = disp.hwLayers[iOverlay];
    layerOvl.compositionType = HWC_OVERLAY;
    layerOvl.hints = HWC_HINT_CLEAR_FB;

    // prepInfo_g <-- save info into prepInfo_g to call set_xx() later.
    prepInfo_g.numWin = 1;
    prepInfo_g.win[0].idWin = prepInfo_g.win[0].zOrder = isBottom ? 0 : 1;
    prepInfo_g.win[0].pLayer = &disp.hwLayers[iOverlay];
    prepInfo_g.prepareOverlay( iOverlay );
    prepInfo_g.prepareRedraw( needReDraw );
    if ( needReDraw || numLayer >= 2 ) {
        hwc_layer_1_t&    layerFB = disp.hwLayers[numLayer];
        prepInfo_g.numWin = 2;
        prepInfo_g.win[1].idWin = prepInfo_g.win[0].idWin ^ 1;
        prepInfo_g.win[1].zOrder = prepInfo_g.win[0].zOrder ^ 1;
        prepInfo_g.win[1].pLayer = &layerFB;
    }

    return 0;
}

static int set_vop_gpu_reuse_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    return set_vop_policy( context, disp, geo );
}

/*
  In this policy:
    all layers are drawn with gpu ( actually by SurfaceFlinger ).


  Action( on set_xx_policy() side ):
  make frame buffer target visible --> winx, 
  let SurfaceFlinger draw all layers onto the next frame's frame buffer target with GPU .
*/
static int try_gpu_policy( hwcContext&, hwc_display_contents_1_t& disp, LayerGeometry const& )
{
    int const   numLayer = disp.numHwLayers - 1;
    for ( int i = 0; i < numLayer ; i++ ) {
        disp.hwLayers[i].compositionType = HWC_FRAMEBUFFER;
    }

    prepInfo_g.numWin = 1;
    prepInfo_g.win[0].idWin = 0;
    prepInfo_g.win[0].zOrder = 0;
    prepInfo_g.win[0].pLayer = &disp.hwLayers[numLayer];

    return 0;
}

static int set_gpu_policy( hwcContext& context, hwc_display_contents_1_t& disp, LayerGeometry const& geo )
{
    return set_vop_policy( context, disp, geo );
}



//======================================
// hwc_xxx
//======================================
//return property value of pcProperty
int hwc_get_int_property(const char* pcProperty, const char* default_value)
{
    char value[PROPERTY_VALUE_MAX];
    int new_value = 0;

    if (pcProperty == NULL || default_value == NULL)
    {
        ALOGE("hwc_get_int_property: invalid param");
        return -1;
    }

    property_get(pcProperty, value, default_value);
    new_value = atoi(value);

    return new_value;
}

static void hwc_sync( hwc_display_contents_1_t& disp )
{
    if ( &disp == NULL) {
        return ;
    }
    for (  int i = 0; i < (int)disp.numHwLayers; i++ ) {
        if (disp.hwLayers[i].acquireFenceFd > 0) {
            sync_wait(disp.hwLayers[i].acquireFenceFd, 500);
        }
    }
}

static void hwc_sync_release( hwc_display_contents_1_t& disp )
{
    for ( int i = 0; i < (int)disp.numHwLayers; i++) {
        hwc_layer_1_t& layer = disp.hwLayers[i];
        if ( &layer == NULL ) {
            return ;
        }
        if (layer.acquireFenceFd > 0) {
            close(layer.acquireFenceFd);
            layer.acquireFenceFd = -1;  //disp.hwLayers[i].acquireFenceFd = -1;
        }
    }
    if (disp.outbufAcquireFenceFd > 0) {
        close(disp.outbufAcquireFenceFd);
        disp.outbufAcquireFenceFd = -1;
    }
}

static uint32_t hwc_get_vsync_period_from_fb_fps()//@@@@int fbindex)
{
  uint32_t  period = 1000000000 / 60; // default
  const char node[] = "/sys/class/graphics/fb0/fps";
  int fbFd = open(node, O_RDONLY);

  if(fbFd > -1) {
    char value[100];
    int ret = read(fbFd, value, sizeof(value)-1);
    if(ret <= 0) {
      ALOGE("fb0/fps read fail %s", strerror(errno));
    } else {
      value[ret] = 0;
      int fps = 0;
      sscanf(value, "fps:%d", &fps);
      ALOGI("fb0/fps fps is:%d", fps);
      if ( fps > 0 )
        period = (uint32_t)(1000000000 / fps);
    }
    close(fbFd);
  }

  return period;
}

//======================================
// hwc_xxx
//======================================
// do base check ,all policy can not support except gpu

static int try_prepare_first( hwcContext& context, hwc_display_contents_1_t& disp )
{
    for ( int i = 0; i < (int)disp.numHwLayers - 1 ; i++ ) {
        hwc_layer_1_t&  layer = disp.hwLayers[i];
        
        if( &layer ) {
            struct private_handle_t const&  handle = privateHandle( layer.handle );
            HWC_LOG(
                (ALOGD("layer[%d],hanlde=%p,tra=%d,flag=%d",i,&handle,layer.transform,layer.flags),
                 &handle
                 ? (ALOGD("layer[%d],fmt=%d,usage=%x,protect=%x",i,handle.format,handle.usage,GRALLOC_USAGE_PROTECTED),0)
                 : 0
                    ));
            if ( &handle && handle.type && !context.iommuEn ) {
                HWC_LOG(ALOGW("kernel iommu disable,but use mmu buffer,so hwc can not support!"));
                return -1;
            }
            if ( &handle && handle.format == HAL_PIXEL_FORMAT_YV12 ) {   
                HWC_LOG(ALOGW("line=%d",__LINE__));
                return -1;
            }
        }    
    }
    if ((disp.numHwLayers - 1) <= 0 || disp.numHwLayers > RGA_REL_FENCE_NUM) {  // vop not support
        HWC_LOG(ALOGW("line=%d,num=%d",__LINE__,disp.numHwLayers));
        return -1;
    }

    int hwc_en = hwc_get_int_property("sys.hwc.enable","1");
    if ( !hwc_en ) {
        HWC_LOG(ALOGW("line=%d",__LINE__));
        return -1;
    }
    return 0;
}

static int hwc_prepare_primary( hwc_composer_device_1* dev, hwc_display_contents_1_t& disp ) 
{
    hwcContext& context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];

    DEBUG_CODE(
        debugInfo_g.save( disp );
        )

    // Check device handle. 
    if ( &context == NULL || &context.device.common != (hw_device_t*)dev ) {
        LOGE("%s(%d):Invalid device!", __FUNCTION__, __LINE__);
        return HWC_EGL_ERROR;
    }

    // at boot time something irregular happens.
    if ( &disp == 0 || disp.numHwLayers == 0 || disp.hwLayers[disp.numHwLayers-1].handle == 0 ) {
        int const             numLayer = &disp ? disp.numHwLayers : -1;
        hwc_layer_1_t const&  layerFB = numLayer > 0 ? disp.hwLayers[numLayer-1] : *(hwc_layer_1_t const*)0;
        private_handle_t const& handle = &layerFB ? privateHandle( layerFB.handle ) : *(private_handle_t const*)0;
        int const             type = &layerFB ? layerFB.compositionType : -1;
        ALOGD("@@@@ numLayer=%d, layer=%p, handle=%p, type=%d", numLayer, &layerFB, &handle, type );

        prepInfo_g.invalidate();
        return 0;
    }
    
    LayerGeometry&  geo = *(LayerGeometry*)context.pLayerGeometry;
    geo.update( disp );

    // try each policy
    int iStart = try_prepare_first( context, disp ) ? NELEM(policy_table_primary)-1 : 0;
    for ( int i = iStart ; i < NELEM(policy_table_primary) ; i ++ ) {
        policy_entry const& ent = policy_table_primary[i];
        if ( ent.try_policy_fnc( context, disp, geo ) == 0 ) {
            prepInfo_g.preparePolicy( ent );
            break;
        }
    }
    /* */
    return 0;
}

int hwc_prepare_virtual( hwc_composer_device_1 */*dev*/, hwc_display_contents_1_t& disp, hwc_display_contents_1_t& disp_P) 
{
    if (NULL  == &disp || NULL == &disp_P) {
      HWC_LOG(ALOGD("disp=%p,disp_p=%p",&disp,&disp_P));
        return 0;
    }
    return 0;
}

static int hwc_prepare( hwc_composer_device_1_t*dev, size_t numDisplays, hwc_display_contents_1_t** displays )
{

    int ret = 0;

    // Check device handle. 
    HWC_LOG_START;  //@@@@ stop at end of hwc_set()
    HWC_LOG(ALOGD("-----------hwc_prepare_start,numDisplays=%d -----------------",numDisplays));

    for ( int i = 0 ; i < (int)numDisplays ; i++ ) {
        hwc_display_contents_1_t& disp = *displays[i];
        switch( i ) {
        case HWC_DISPLAY_PRIMARY:
            ret = hwc_prepare_primary(dev, disp);
            break;
        case HWC_DISPLAY_EXTERNAL:
            break;
        case HWC_DISPLAY_VIRTUAL:
            ret = hwc_prepare_virtual( dev, disp , *displays[HWC_DISPLAY_PRIMARY] );
            break;
        default:
            ret = -EINVAL;
        }
    }
    return ret;
}


static int hwc_blank( struct hwc_composer_device_1*/*dev*/, int dpy, int blank )
{
    // We're using an older method of screen blanking based on
    // early_suspend in the kernel.  No need to do anything here.
    hwcContext& context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];

    ALOGI("dpy=%d,blank=%d",dpy,blank);
    switch ( dpy ) {
    case HWC_DISPLAY_PRIMARY:
    {
        prepInfo_g.invalidate();
        
        int fb_blank = blank ? FB_BLANK_POWERDOWN : FB_BLANK_UNBLANK;
        int err = ioctl(context.fbFd, FBIOBLANK, fb_blank);
        ALOGD("call fb blank =%d",fb_blank);
        if (err < 0) {
            if (errno == EBUSY)
                ALOGD("%sblank ioctl failed (display already %sblanked)",
                      blank ? "" : "un", blank ? "" : "un");
            else
                ALOGE("%sblank ioctl failed: %s", blank ? "" : "un",
                      strerror(errno));
            return -errno;
        } else {
            context.fb_blanked = blank;
        }
        break;
    }

    case HWC_DISPLAY_EXTERNAL:
        break;

    default:
        return -EINVAL;
    }
    return 0;
}

int hwc_query(struct hwc_composer_device_1* /*dev*/, int what, int* value)
{
    hwcContext& context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];

    switch ( what )     {
    case HWC_BACKGROUND_LAYER_SUPPORTED:
        // we support the background layer
        value[0] = 1;
        break;
    case HWC_VSYNC_PERIOD:
        // vsync period in nanosecond
        //value[0] = 1e9 / context.fb_fps;
        value[0] = context.dpyAttr[HWC_DISPLAY_PRIMARY].vsync_period;
        HWC_LOG(ALOGD("get primary period=%d", *value));
        break;
    default:
        // unsupported query
        return -EINVAL;
    }
    return 0;
}


int hwc_set_virtual(hwc_composer_device_1_t* /*dev*/, hwc_display_contents_1_t  **contents)
{
    hwc_display_contents_1_t* list_pri = contents[0];
    hwc_display_contents_1_t* list_wfd = contents[2];
    hwc_layer_1_t *  fbLayer = &list_pri->hwLayers[list_pri->numHwLayers - 1];
    hwc_layer_1_t *  wfdLayer = NULL;
    //hwcContext * context = gcontextAnchor[HWC_DISPLAY_PRIMARY];
    HWC_TIME_DECLARE;
    HWC_TIME_BEGIN;

    if(list_pri== NULL || list_wfd == NULL) {
        return -1;
    }
    wfdLayer = &list_wfd->hwLayers[list_wfd->numHwLayers - 1];
    
    if (fbLayer == NULL || wfdLayer == NULL) {
        return -1;
    }
    
    if (list_wfd) {
        hwc_sync( *list_wfd );
    }

    //@@@@ for test
    /* (org)
    int const type = hwc_get_int_property("sys.enable.wfd.optimize", "0");//@@@@
    if ((type  > 0) && wfdLayer->handle) {  //@@@@
      //@@@@(org) if ((context.wfdOptimize > 0) && wfdLayer->handle) {
        hwc_cfg_t cfg;
        memset(&cfg, 0, sizeof(hwc_cfg_t));
        cfg.src_handle = reinterpret_cast<struct private_handle_t const*>(fbLayer->handle);
        cfg.transform = 0; //@@@@fbLayer->realtransform;

        cfg.dst_handle = reinterpret_cast<struct private_handle_t const*>(wfdLayer->handle);
        cfg.src_rect.left = (int)fbLayer->displayFrame.left;
        cfg.src_rect.top = (int)fbLayer->displayFrame.top;
        cfg.src_rect.right = (int)fbLayer->displayFrame.right;
        cfg.src_rect.bottom = (int)fbLayer->displayFrame.bottom;
        //cfg.src_format = cfg.src_handle->format;

        cfg.rga_fbFd = context.engine_fd;
        cfg.dst_rect.left = (int)wfdLayer->displayFrame.left;
        cfg.dst_rect.top = (int)wfdLayer->displayFrame.top;
        cfg.dst_rect.right = (int)wfdLayer->displayFrame.right;
        cfg.dst_rect.bottom = (int)wfdLayer->displayFrame.bottom;
        //cfg.dst_format = cfg.dst_handle->format;
        set_rga_cfg(&cfg);
        do_rga_transform_and_scale();
    }
    */

    if (list_wfd) {
        hwc_sync_release( *list_wfd );
    }

    HWC_TIME_END(16,ALOGD("hwc use time=%ld ms", hwc_diff));
    return 0;
}


static int hwc_set_primary(hwc_composer_device_1 *dev, hwc_display_contents_1_t&disp) 
{
    hwcContext&   context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];
   // Check device handle. 
    if ( &context == NULL || &context.device.common != (hw_device_t*)dev ) {
        LOGE("%s(%d): Invalid device!", __FUNCTION__, __LINE__);
        return hwcRGA_OPEN_ERR;
    }

    if ( ! prepInfo_g.isPrepared() ) {
        ALOGE("@@@@ not prepared" );
        hwc_sync_release( disp );
        return 0;
    }

    DEBUG_CODE(
        if ( debugInfo_g.check( disp ) < 0 ) {
            ALOGE("@@@@ policy=%p(%s)", prepInfo_g.pPolicyLast,
                  isValidPolicy(*prepInfo_g.pPolicyLast) ? prepInfo_g.pPolicyLast->strCompose : "invalid" );
        }
        
        );

    LayerGeometry const&  geo = *(LayerGeometry const*)context.pLayerGeometry;
    policy_entry const&   policy = prepInfo_g.executePolicy();
    if ( &policy ) {
        policy.set_policy_fnc( context, disp, geo );
    } else {
        hwc_sync_release( disp );
        ALOGE("@@@@ %d,fatal",__LINE__);
        return -EINVAL;
    }
    /**/

    hwc_sync_release( disp );
    return 0; //? 0 : HWC_EGL_ERROR;
}

static int hwc_set(
    hwc_composer_device_1_t * dev,
    size_t numDisplays,
    hwc_display_contents_1_t  ** displays
    )
{

    int ret = 0;
    for (uint32_t i = 0; i < numDisplays; i++) {
        hwc_display_contents_1_t& disp = *displays[i];
        switch( i ) {
            case HWC_DISPLAY_PRIMARY:
                ret = hwc_set_primary(dev, disp);
                break;
            case HWC_DISPLAY_EXTERNAL:
              //@@@@ret = hwc_set_external(dev, list);
                break;
            case HWC_DISPLAY_VIRTUAL:           
                ret = hwc_set_virtual( dev, displays );
                break;
            default:
                ret = -EINVAL;
        }
    }

    DEBUG_CODE( HWC_LOG_STOP; )
    return ret;
}

static void hwc_registerProcs(struct hwc_composer_device_1* /*dev*/,
                              hwc_procs_t const* procs)
{
    hwcContext& context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];
    context.procs = (hwc_procs_t *)procs;
}


static int hwc_event_control(struct hwc_composer_device_1*, int /*dpy*/, int event, int enabled)
{

    hwcContext& context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];

    switch ( event ) {
    case HWC_EVENT_VSYNC:
    {
        int val = !!enabled;
        int err;
        err = ioctl(context.fbFd, RK_FBIOSET_VSYNC_ENABLE, &val);
        if (err < 0)
            {
                LOGE(" RK_FBIOSET_VSYNC_ENABLE err=%d", err);
                return -1;
            }
        return 0;
    }
    default:
        return -1;
    }
}

// use epoll( edge triger mode )
static void*  hwc_thread(void *)
{
  int rc = -1;
  const char* msg = 0;
  hwcContext& context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];

  errno = 0;
  prctl(PR_SET_NAME,"HWC_vsync");

  // timeLast <--
  struct timespec timeLast;
  clock_gettime( CLOCK_MONOTONIC, &timeLast );

  // fdEpol <-- setup epoll
  struct epoll_event  evConfig = { EPOLLPRI | EPOLLET, {u32:0} };
  int fdEpol = epoll_create(1);
  msg="epoll_create"; if (fdEpol<0) goto err;
  rc = epoll_ctl( fdEpol, EPOLL_CTL_ADD, context.vsync_fd, &evConfig );
  msg="epoll_ctl"; if (rc<0) goto err;

  // event loop
  while (true) {
    // calling epoll( event=POLLPRI, timeout=-1 ) returns if VSYNC is enabled,
    // otherwise(vsync disabled) it does not return.
    struct epoll_event evRet = { 0, {u32:0} };
    errno = 0;
    rc = epoll_wait(fdEpol, &evRet, 1, -1);

    // break if interrupted
    if (rc < 0) {
      if (errno == EINTR)
        break;
      ALOGE("error vsync thread epoll_wait: rc=%d %s", rc, strerror(errno));
      continue;
    }

#if 0
    DEBUG_CODE0(
          {
          static int cnt=0;
          uint64_t t = (uint64_t)timeLast.tv_sec * 1E9 + (uint64_t)timeLast.tv_nsec;
          if ( ((++cnt) & 0x3f) == 0 ) ALOGD("@@@@ vsync %lld@@@@", t);
          }
        )
#endif

    // call procs->vsync if the time advanced.
    if (rc > 0 && ( evRet.events & POLLPRI ) && context.procs) {
      
      struct timespec timeCurr;
      if ( clock_gettime( CLOCK_MONOTONIC, &timeCurr ) == 0 &&
           ( timeCurr.tv_nsec != timeLast.tv_nsec || timeCurr.tv_sec != timeLast.tv_sec ) ) {
        
        int64_t  timestamp = (int64_t)timeCurr.tv_sec * 1E9 + (int64_t)timeCurr.tv_nsec;
        context.procs->vsync(context.procs, HWC_DISPLAY_PRIMARY, timestamp);
        timeLast = timeCurr;
      }
    } 
  }

 err:
  if ( msg ) ALOGE("error vsync thread rc=%d (%s) (%s)", rc, msg, strerror(errno));
  if ( fdEpol >= 0 ) close( fdEpol );
  return NULL;
}

static int hwc_device_close( struct hw_device_t *dev )
{
    hwcContext&   context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];
    int err;
    LOGD("%s(%d):Close hwc device in thread=%d",
         __FUNCTION__, __LINE__, gettid());

    /* Check device. */
    if ( &context == NULL || &context.device.common != (hw_device_t*)dev ) {
        LOGE("%s(%d):Invalid device!", __FUNCTION__, __LINE__);
        return -EINVAL;
    }

    if ( --context.reference > 0) {
        /* Dereferenced only. */
        return 0;
    }

    if ( context.engine_fd )
        close(context.engine_fd);
    /* Clean context. */
    if ( context.vsync_fd > 0 )
        close(context.vsync_fd);
    if ( context.fbFd > 0 ) 
        close(context.fbFd);
    if (context.ippDev) 
        ipp_close(context.ippDev);

    for ( int i = 0 ; i < FB_BUFFERS_NUM ; i++ ) {
        if ( context.phd_bk[i] ) {
            err = context.mAllocDev->free(context.mAllocDev, context.phd_bk[i]);
            ALOGW_IF(err, "free(...) failed %d (%s)", err, strerror(-err));
        }
    }
    if ( context.pLayerGeometry )
        delete (LayerGeometry*)context.pLayerGeometry;


    //@@@@ pthread_mutex_destroy(&context.lock);
    free( &context );
    gcontextAnchor[HWC_DISPLAY_PRIMARY] = NULL;
    return 0;
}

static int hwc_getDisplayConfigs(struct hwc_composer_device_1*, int disp,
                                 uint32_t* configs, size_t* numConfigs)
{
  int ret = 0;
  //in 1.1 there is no way to choose a config, report as config id # 0
  //This config is passed to getDisplayAttributes. Ignore for now.
  switch (disp) {
  case HWC_DISPLAY_PRIMARY:
    if (*numConfigs > 0)
      {
        configs[0] = 0;
        *numConfigs = 1;
      }
    ret = 0; //NO_ERROR
    break;
  case HWC_DISPLAY_EXTERNAL:
    ret = -1; //Not connected
    //@@@@
    /*
      if (pdev->dpyAttr[HWC_DISPLAY_EXTERNAL].connected)
      {
      ret = 0; //NO_ERROR
      if (*numConfigs > 0)
      {
      configs[0] = 0;
      *numConfigs = 1;
      }
      }
    */
    break;
            
  case HWC_DISPLAY_VIRTUAL: //@@@@ not implemented
    ret = -1;
    break;

  }
  return ret;
}

static int hwc_getDisplayAttributes(struct hwc_composer_device_1* dev, int disp,
                                    uint32_t config, const uint32_t* attributes, int32_t* values)
{
    HWC_UNREFERENCED_PARAMETER(config);

    hwcContext  *pdev = (hwcContext  *)dev;
    //If hotpluggable displays are inactive return error
    if (disp == HWC_DISPLAY_EXTERNAL && !pdev->dpyAttr[disp].connected)
    {
        return -1;
    }
    static  uint32_t DISPLAY_ATTRIBUTES[] =
    {
        HWC_DISPLAY_VSYNC_PERIOD,
        HWC_DISPLAY_WIDTH,
        HWC_DISPLAY_HEIGHT,
        HWC_DISPLAY_DPI_X,
        HWC_DISPLAY_DPI_Y,
        HWC_DISPLAY_NO_ATTRIBUTE,
    };
    //From HWComposer

    const int NUM_DISPLAY_ATTRIBUTES = (sizeof(DISPLAY_ATTRIBUTES) / sizeof(DISPLAY_ATTRIBUTES)[0]);

    for (size_t i = 0; i < NUM_DISPLAY_ATTRIBUTES - 1; i++)
    {
        switch (attributes[i])
        {
            case HWC_DISPLAY_VSYNC_PERIOD:
                values[i] = pdev->dpyAttr[disp].vsync_period;
                break;
            case HWC_DISPLAY_WIDTH:
                values[i] = pdev->dpyAttr[disp].xres;
                ALOGD("%s disp = %d, width = %d", __FUNCTION__, disp,
                      pdev->dpyAttr[disp].xres);
                break;
            case HWC_DISPLAY_HEIGHT:
                values[i] = pdev->dpyAttr[disp].yres;
                ALOGD("%s disp = %d, height = %d", __FUNCTION__, disp,
                      pdev->dpyAttr[disp].yres);
                break;
            case HWC_DISPLAY_DPI_X:
                values[i] = (int32_t)(pdev->dpyAttr[disp].xdpi);
                break;
            case HWC_DISPLAY_DPI_Y:
                values[i] = (int32_t)(pdev->dpyAttr[disp].ydpi);
                break;
            default:
                ALOGE("Unknown display attribute %d",
                      attributes[i]);
                return -EINVAL;
        }
    }

    return 0;
}

//@@@@int is_surport_wfd_optimize()
#if 0
int is_support_wfd_optimize()
{
    char value[PROPERTY_VALUE_MAX];
    memset(value, 0, PROPERTY_VALUE_MAX);
    property_get("drm.service.enabled", value, "false");
    if (!strcmp(value, "false")) {
        return false;
    } else {
        return true;
    }
}
#endif

// dump info about primary display.
static void hwc_dump( struct hwc_composer_device_1*, char* buff, int len )
{
    hwcContext& context = *gcontextAnchor[HWC_DISPLAY_PRIMARY];
    LayerGeometry const&  geo = *(LayerGeometry const*)context.pLayerGeometry;
    int       rc;

    DUMP0( buff, len, "\n" );
    DUMPX( debugInfo_g.dump( buff, len ) );
    DUMP0( buff, len, "\n" );
    DUMPX( geo.dump( buff,len ) );
    DUMP0( buff, len, "\n" );
    DUMPX( prepInfo_g.dump( buff, len ) );
    DUMP0( buff, len, "\n" );

  ret:;
}

static int hwc_device_open(
    const struct hw_module_t * module,
    const char * name,
    struct hw_device_t ** device
)
{
    int  status    = 0;
    int rel;
    int err;
    struct fb_fix_screeninfo fixInfo;
    struct fb_var_screeninfo info;
    float xdpi;
    float ydpi;
    int stride_gr;


    LOGD("%s(%d):Open hwc device in thread=%d", __FUNCTION__, __LINE__, gettid());

    *device = NULL;

    if (strcmp(name, HWC_HARDWARE_COMPOSER) != 0) {
        LOGE("%s(%d):Invalid device name!", __FUNCTION__, __LINE__);
        return -EINVAL;
    }

    // Return if already initialized. 
    hwcContext* pContext = gcontextAnchor[HWC_DISPLAY_PRIMARY]; //@@@@NULL;
    if ( pContext ) {
        hwcContext& context = *pContext;
        // Increament reference count. 
        context.reference++;

        *device = &context.device.common;
        return 0;
    }
   
    // Allocate memory. 
    hwcContext& context = *(hwcContext*)malloc(sizeof(hwcContext));
    //context = (hwcContext *) malloc(sizeof(hwcContext));
    if ( &context == NULL ) {
        LOGE("%s(%d):malloc Failed!", __FUNCTION__, __LINE__);
        return -EINVAL;
    }
    memset( &context, 0, sizeof(hwcContext));

    //@@@@???? To get fixInfo and info from "dev/....", it's mandetory to execute 'init_frame_buffer()' in SurfaceFlinger.
    context.fbFd = open("/dev/graphics/fb0", O_RDWR, 0);
    if (context.fbFd < 0) {  //@@@@???? If failed, we should try "/dev/fb0".
        hwcONERROR(hwcSTATUS_IO_ERR);
    }
    rel = ioctl(context.fbFd, FBIOGET_FSCREENINFO, &fixInfo);
    if (rel != 0) {
        hwcONERROR(hwcSTATUS_IO_ERR);
    }
    if (ioctl(context.fbFd, FBIOGET_VSCREENINFO, &info) == -1) {
        hwcONERROR(hwcSTATUS_IO_ERR);
    }

    xdpi = 1000 * (info.xres * 25.4f) / info.width;
    ydpi = 1000 * (info.yres * 25.4f) / info.height;


    context.dpyAttr[HWC_DISPLAY_PRIMARY].fd = context.fbFd;
    //xres, yres may not be 32 aligned

    //@@@@???? Should be "stride = fixInfo.line_length / (info.bits_per_pixel>>3);"
    context.dpyAttr[HWC_DISPLAY_PRIMARY].stride = fixInfo.line_length / (info.xres / 8);
    context.dpyAttr[HWC_DISPLAY_PRIMARY].xres = info.xres;
    context.dpyAttr[HWC_DISPLAY_PRIMARY].yres = info.yres;
    context.dpyAttr[HWC_DISPLAY_PRIMARY].xdpi = xdpi;
    context.dpyAttr[HWC_DISPLAY_PRIMARY].ydpi = ydpi;
    context.dpyAttr[HWC_DISPLAY_PRIMARY].vsync_period = hwc_get_vsync_period_from_fb_fps();//@@@@vsync_period;
    context.dpyAttr[HWC_DISPLAY_PRIMARY].connected = true;

    // Initialize variables. 

    context.device.common.tag = HARDWARE_DEVICE_TAG;
    context.device.common.version = HWC_DEVICE_API_VERSION_1_3;
    context.device.common.module  = (hw_module_t *) module;

    // initialize the procs 
    context.device.common.close   = hwc_device_close;
    context.device.prepare        = hwc_prepare;
    context.device.set            = hwc_set;
    // context.device.common.version = HWC_DEVICE_API_VERSION_1_0;
    context.device.blank          = hwc_blank;
    context.device.query          = hwc_query;
    context.device.eventControl   = hwc_event_control;

    context.device.registerProcs  = hwc_registerProcs;

    context.device.getDisplayConfigs = hwc_getDisplayConfigs;
    context.device.getDisplayAttributes = hwc_getDisplayAttributes;
    context.device.dump = hwc_dump;

    context.membk_index = 0;

    context.engine_fd = open("/dev/rga", O_RDWR, 0);
    if (context.engine_fd < 0) {
	    hwcONERROR(hwcRGA_OPEN_ERR);
    }

    //@@@@#if ENABLE_WFD_OPTIMIZE
    //@@@@    property_set("sys.enable.wfd.optimize", "1");
    //@@@@#endif
    //@@@@if (context.engine_fd > 0) {
    //@@@@  int const type = hwc_get_int_property("sys.enable.wfd.optimize", "0");
    //@@@@  context.wfdOptimize = type;
    //@@@@  if (type > 0 && !is_support_wfd_optimize() ) {
    //@@@@        property_set("sys.enable.wfd.optimize", "0");
    //@@@@  }
    //@@@@}
    //@@@@property_set("sys.yuv.rgb.format", "4");

    context.fbhandle.width = info.xres;
    context.fbhandle.height = info.yres;
    context.fbhandle.format = info.nonstd & 0xff;
    context.fbhandle.stride = (info.xres + 31) & (~31);

    if (context.engine_fd > 0) {
        if ( hw_get_module(GRALLOC_HARDWARE_MODULE_ID, &context.module_gr) == 0 &&
           gralloc_open(context.module_gr, &context.mAllocDev) == 0 ) {

            for (int  i = 0;i < FB_BUFFERS_NUM;i++) {
                err = context.mAllocDev->alloc(context.mAllocDev,rkmALIGN(info.xres,32),
                                                info.yres, context.fbhandle.format,
                                                GRALLOC_USAGE_HW_COMPOSER | GRALLOC_USAGE_HW_RENDER,
                                                (buffer_handle_t*)(&context.phd_bk[i]), &stride_gr);
                if (!err) {
                    struct private_handle_t const&  phandle_gr = privateHandle( context.phd_bk[i] );
                    ALOGD("@hwc alloc [%dx%d,f=%d],fd=%d", phandle_gr.width, phandle_gr.height, phandle_gr.format, phandle_gr.share_fd);
                } else { 
                    ALOGE("hwc alloc faild");
                    goto OnError;
                }
            }
        } else {
            ALOGE(" GRALLOC_HARDWARE_MODULE_ID failed");
            goto OnError;   //@@@@
        }
    }

    /* Increment reference count. */
    context.reference++; 

    gcontextAnchor[HWC_DISPLAY_PRIMARY] = &context;

    rel = ioctl(context.fbFd, RK_FBIOGET_IOMMU_STA, &context.iommuEn);	    
    if (rel != 0) {
         hwcONERROR(hwcSTATUS_IO_ERR);
    }    

#if USE_HW_VSYNC
    context.vsync_fd = open("/sys/class/graphics/fb0/vsync", O_RDONLY, 0);
    if (context.vsync_fd < 0) {
        hwcONERROR(hwcSTATUS_IO_ERR);
    }
    if (pthread_create(&context.vsync_thread, NULL, hwc_thread, &context)) {
        hwcONERROR(hwcTHREAD_ERR);
    }
#endif

    /* Return device handle. */
    *device = &context.device.common;

    
    char acVersion[100];
    memset(acVersion,0,sizeof(acVersion));
    if(sizeof(GHWC_VERSION) > 12)
        strncpy(acVersion,GHWC_VERSION,12);
    else
        strcpy(acVersion,GHWC_VERSION);
#ifndef TARGET_SECVM
    strcat(acVersion,"");
#else
    strcat(acVersion,"_sec");
#endif
#if HOTPLUG_MODE
    strcat(acVersion,"_htg");
#else
    strcat(acVersion,"");
#endif
    strcat(acVersion,"");
    strcat(acVersion,RK_GRAPHICS_VER);
    property_set("sys.ghwc.version", acVersion);

    LOGD(RK_GRAPHICS_VER);

    if (context.engine_fd > 0) {
        char Version[32];
        memset(Version, 0, sizeof(Version));
        if (ioctl(context.engine_fd, RGA_GET_VERSION, Version) == 0) {
            property_set("sys.grga.version", Version);
            LOGD(" rga version =%s", Version);
        }
    } else {
        LOGD(" rga not exit");
    }
    context.ippDev = new ipp_device_t();
    rel = ipp_open(context.ippDev);
    if (rel < 0) {
        delete context.ippDev;
        context.ippDev = NULL;
        ALOGE("Open ipp device fail.");
    }


    context.pLayerGeometry = (void*)( new LayerGeometry() );
    
    return 0;

OnError:
    /* Error roll back. */
    if ( &context != NULL ) {

        if ( context.ippDev )
            delete context.ippDev;
        if (context.engine_fd != 0) 
            close(context.engine_fd);
        if (context.vsync_fd > 0) 
            close(context.vsync_fd);
        if (context.fbFd > 0) 
            close(context.fbFd);
        for ( int i = 0 ; i < FB_BUFFERS_NUM ; i++ ) {
            if (context.phd_bk[i]) {
                    err = context.mAllocDev->free(context.mAllocDev, context.phd_bk[i]);
                    ALOGW_IF(err, "free(...) failed %d (%s)", err, strerror(-err));
                }
        }
        if ( context.pLayerGeometry )
            delete (LayerGeometry*)context.pLayerGeometry;


        free( &context );
    }

    *device = NULL;
    LOGE("%s(%d):Failed!", __FUNCTION__, __LINE__);
    return -EINVAL;
}





