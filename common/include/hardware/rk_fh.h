
#ifndef __RK_FH_H___
#define __RK_FH_H___

#define RK30_MAX_LCDC_SUPPORT	2
#define RK30_MAX_LAYER_SUPPORT	5
#define RK_MAX_FB_SUPPORT       5
#define RK_WIN_MAX_AREA		    4
#define RK_MAX_BUF_NUM       	11

//@@@@
//@@@@ Definition of 'u32' here conflicts with sys/epoll.h which uses u32 as struct's member.
//@@@@ so rename it to uint32_t( also u8 and u16 is renamed ).
/*
#define u32 unsigned int
#define u8  unsigned char
#define u16 unsigned  short
*/

/*ALPHA BLENDING MODE*/
enum alpha_mode {               /*  Fs       Fd */
	AB_USER_DEFINE     = 0x0,
	AB_CLEAR    	   = 0x1,/*  0          0*/
	AB_SRC      	   = 0x2,/*  1          0*/
	AB_DST    	       = 0x3,/*  0          1  */
	AB_SRC_OVER   	   = 0x4,/*  1   	    1-As''*/
	AB_DST_OVER    	   = 0x5,/*  1-Ad''   1*/
	AB_SRC_IN    	   = 0x6,
	AB_DST_IN    	   = 0x7,
	AB_SRC_OUT    	   = 0x8,
	AB_DST_OUT    	   = 0x9,
	AB_SRC_ATOP        = 0xa,
	AB_DST_ATOP    	   = 0xb,
	XOR                = 0xc,
	AB_SRC_OVER_GLOBAL = 0xd
}; /*alpha_blending_mode*/

struct rk_lcdc_post_cfg{
	uint32_t xpos;
	uint32_t ypos;
	uint32_t xsize;
	uint32_t ysize;
};

struct rk_fb_area_par {
	uint8_t  data_format;        /*layer data fmt*/
	short ion_fd;
	unsigned int phy_addr;
	short acq_fence_fd;
	uint16_t  x_offset;
	uint16_t  y_offset;
	uint16_t xpos;		/*start point in panel  --->LCDC_WINx_DSP_ST*/
	uint16_t ypos;
	uint16_t xsize;		/* display window width/height  -->LCDC_WINx_DSP_INFO*/
	uint16_t ysize;
	uint16_t xact;		/*origin display window size -->LCDC_WINx_ACT_INFO*/
	uint16_t yact;
	uint16_t xvir;		/*virtual width/height     -->LCDC_WINx_VIR*/
	uint16_t yvir;
	uint8_t  fbdc_en;
	uint8_t  fbdc_cor_en;
	uint8_t  fbdc_data_format;
	uint16_t data_space;
	uint32_t reserved0;
};

struct rk_fb_win_par {
	uint8_t  win_id;
	uint8_t  z_order;		/*win sel layer*/
	uint8_t  alpha_mode;
	uint16_t g_alpha_val;
	uint8_t  mirror_en;
	struct rk_fb_area_par area_par[RK_WIN_MAX_AREA];
	uint32_t reserved0;
};

struct rk_fb_win_cfg_data {
	uint8_t  wait_fs;
	short ret_fence_fd;
	short rel_fence_fd[RK_MAX_BUF_NUM];
	struct  rk_fb_win_par win_par[RK30_MAX_LAYER_SUPPORT];
	struct  rk_lcdc_post_cfg post_cfg;
};

#endif /* __RK_FH_H___ */

