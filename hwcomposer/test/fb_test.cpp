#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdlib.h>
#include <sys/mman.h>

#include  <stdio.h>
#include  <time.h>


//#define PAGE_SIZE 4096
inline size_t round_up_to_page_size(size_t x)
{
	return (x + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
}

int main(int argc, const char*argv[] )
{
  int       errLine = -1;
  int		    fdFB = 0;
  int const sizeFB = 600*1024*4;  // rgba

  int       index;

  int*      pBuff; 
  size_t    sizeFBALL;
	void *    vaddr;
  //TIME_DECLARE(t);


  // index <-- cmd line
  if ( argc != 2 ) {
    printf("usage : %s <index(0..2)>", argv[0] );
		errLine = __LINE__; goto ret;
  }
  index = atoi( argv[1] );
  if ( index < 0 || index > 2 ) {
		errLine = __LINE__; goto ret;
  }


  fdFB = open("/dev/graphics/fb0", O_RDWR, 0);
	if ( fdFB <= 0 ) {
		errLine = __LINE__; goto ret;
	}

  sizeFBALL = round_up_to_page_size(sizeFB*3);
	vaddr = mmap(0, sizeFBALL, PROT_READ | PROT_WRITE, MAP_SHARED, fdFB, 0);
	if (vaddr == MAP_FAILED) 	{
		errLine = __LINE__; goto ret;
	}

  
  // clear fb(partially)
  //for ( int iFB = 0 ; iFB <= 2 ; iFB ++ ) {
  //pBuff = (int*)((intptr_t)vaddr + sizeFB * iFB ); 
    pBuff = (int*)((intptr_t)vaddr + sizeFB * index ); 
    for ( int cnt = 600*1024 / 4 ; -- cnt >= 0 ; ) {
      *pBuff ++ = 0xffff0000;
    }
    //}


  
 ret:
  if ( errLine > 0 ) {
    printf("error: line=%d, %s\n", errLine, strerror(errno) );
  } else
    printf("OK\n");
  if ( fdFB > 0 )
    close(fdFB);

  return 0;
}

